/* eslint-disable no-restricted-syntax */
import { Helmet } from 'react-helmet';
import {
  Backdrop,
  Box,
  CircularProgress,
  Container,
  Grid,
} from '@material-ui/core';
import Sales from 'src/components/dashboard//Sales';
import TasksProgress from 'src/components/dashboard//TasksProgress';
import TotalCustomers from 'src/components/dashboard//TotalCustomers';
import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import dashboardService from '../services/Elections/dashboard-service';

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
}));

const data = {
  datasets: [
    {
      backgroundColor: '#D1B059',
      data: [18, 5, 19, 27, 29, 19, 20],
      label: 'This year'
    },
  ],
  labels: ['1 Aug', '2 Aug', '3 Aug', '4 Aug', '5 Aug', '6 Aug']
};

const Dashboard = () => {
  const classes = useStyles();

  const [totalElectors, setTotalElectors] = useState(0);
  const [electoralParticipation, setElectoralParticipation] = useState(0);
  const [diaryVotes, setDiaryVotes] = useState(data);
  const [diaryElectoralParticipation, setDiaryElectoralParticipation] = useState(data);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    setTotalElectors(1600);
    dashboardService
      .totalElectors()
      .then((response) => {
        setTotalElectors(response.data);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
    dashboardService
      .electoralParticipation()
      .then((response) => {
        setElectoralParticipation(Math.trunc(response.data * 10000) / 100);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
    dashboardService
      .diaryVotes()
      .then((response) => {
        const labels = [];
        const values = [];
        for (const day of response.data) {
          labels.push(day.label);
          values.push(day.result);
        }
        const dataArray = {
          datasets: [
            {
              backgroundColor: '#D1B059',
              data: values,
              label: 'Votos'
            },
          ],
          labels,
        };

        setDiaryVotes(dataArray);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
    dashboardService
      .diaryElectoralParticipation()
      .then((response) => {
        const labels = [];
        const values = [];
        for (const day of response.data) {
          labels.push(day.label);
          values.push(Math.trunc(day.result * 10000) / 100);
        }
        const dataArray = {
          datasets: [
            {
              backgroundColor: '#D1B059',
              data: values,
              label: 'Porcentaje'
            },
          ],
          labels,
        };
        setDiaryElectoralParticipation(dataArray);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
  }, []);

  return (
    <>
      <Helmet>
        <title>Dashboard | Voting System</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth={false}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              lg={6}
              sm={6}
              xl={6}
              xs={12}
            >
              <TotalCustomers label="Total de Electores" numberElements={totalElectors} />
            </Grid>
            <Grid
              item
              lg={6}
              sm={6}
              xl={6}
              xs={12}
            >
              <TasksProgress label="Participación Electoral" percentage={electoralParticipation} />
            </Grid>
            <Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <Sales label="Votaciones Diarias" dataArray={diaryVotes} />
            </Grid>
            <Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <Sales label="Participación Electoral Diaria" dataArray={diaryElectoralParticipation} />
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};

export default Dashboard;

import {
  Box,
  Grid,
  Typography,
  Backdrop,
  CircularProgress,
  TextField,
  Modal,
  Fade,
  Button,
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { ArrowForwardIos } from '@material-ui/icons';
import DatePickerStage from 'src/components/DatePickers/DatePickerStage';
import theme from 'src/theme';
import SnackbarComponent from 'src/components/Snackbars/Snackbar';
import { useNavigate } from 'react-router';
import electionsService from 'src/services/Elections/elections-service';

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
  arrow: {
    cursor: 'pointer',
    backgroundColor: '#851610',
    color: 'FFFFFF',
    borderRadius: '10px',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '10px',
    maxWidth: '90vw',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
    zIndex: 2
  },
}));

const NewElection = (props) => {
  const classes = useStyles();
  const navigate = useNavigate();

  const user = JSON.parse(localStorage.getItem('user'));

  const [newElection, setNewElection] = useState({
    name: '',
    description: '',
    logo: 'none',
    status: 'ACT',
    descriptionInscriptions: '',
    startDateInscriptions: '',
    endDateInscriptions: '',
    descriptionActualization: '',
    startDateActualization: '',
    endDateActualization: '',
    descriptionVote: '',
    startDateVote: '',
    endDateVote: '',
    creationUser: user.id,
  });
  const [stage, setStage] = useState('');
  const [informationModal, setInformationModal] = useState({ startDate: new Date(), endDate: new Date(), description: '' });
  const [dateStart, setDateStart] = useState(new Date());
  const [dateEnd, setDateEnd] = useState(new Date());

  const [open, setOpen] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [message, setMessage] = useState('');
  const [severity, setSeverity] = useState('error');
  const [loading, setLoading] = useState(false);

  const parseDate = (date, functionSet) => {
    const year = parseInt(date.substr(0, 4), 10);
    const month = parseInt(date.substr(5, 7), 10) - 1;
    const day = parseInt(date.substr(8, 10), 10);
    const hour = parseInt(date.substr(11, 13), 10);
    const min = parseInt(date.substr(14, 16), 10);
    const sec = parseInt(date.substr(17, 19), 10);

    functionSet(new Date(year, month, day, hour, min, sec, 0));
  };

  const openModal = (stageSelected) => {
    console.log(stageSelected);

    const inf = {
      startDate: newElection[`startDate${stageSelected}`],
      endDate: newElection[`endDate${stageSelected}`],
      description: newElection[`description${stageSelected}`],
    };

    if (inf.startDate !== '') {
      parseDate(inf.startDate, setDateStart);
    }
    if (inf.endDate !== '') {
      parseDate(inf.endDate, setDateEnd);
    }

    setInformationModal(inf);
    setStage(stageSelected);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseMessage = () => {
    setOpenMessage(false);
  };

  const createElectionService = () => {
    electionsService
      .createElection(newElection)
      .then((response) => {
        if (response.data) {
          setMessage('Registro Exitoso');
          setSeverity('success');
          setOpenMessage(true);
          navigate('/admin/elections', { replace: true });
        }
      })
      .catch((e) => {
        console.log(e);
        setMessage('Error en la operación. Vuelva a intentarlo');
        setSeverity('error');
        setOpenMessage(true);
      });
  };

  const createElection = () => {
    console.log(newElection);
    if (newElection.endDateInscriptions < newElection.startDateActualization && newElection.endDateActualization < newElection.startDateVote) {
      electionsService
        .activeElection()
        .then((response) => {
          if (response.data) {
            if (response.data.endDate < newElection.startDateInscriptions) {
              createElectionService();
            } else {
              setMessage('Ya existe una elección activa en el rango de fechas establecido');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            createElectionService();
          }
        })
        .catch((e) => {
          console.log(e);
          setMessage('Error en la operación. Vuelva a intentarlo');
          setSeverity('error');
          setOpenMessage(true);
        });
    } else {
      setMessage('Las fechas de las etapas deben coherentes. Realizar los cambios respectivos');
      setSeverity('error');
      setOpenMessage(true);
    }
  };

  const saveInformation = () => {
    if (informationModal.startDate < informationModal.endDate) {
      const aux = { ...newElection };

      aux[`startDate${stage}`] = informationModal.startDate;
      aux[`endDate${stage}`] = informationModal.endDate;
      aux[`description${stage}`] = informationModal.description;
      switch (stage) {
        case 'Inscriptions':
          if (newElection.startDateActualization !== '') {
            if (informationModal.endDate < newElection.startDateActualization) {
              setNewElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de fin debe ser menor a la de la etapa de Actualización');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else if (newElection.endDateVote !== '') {
            if (informationModal.endDate < newElection.endDateVote) {
              setNewElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de fin debe ser menor a la de la etapa de Votación');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            setNewElection(aux);
            setOpen(false);
          }
          break;
        case 'Actualization':
          if (newElection.endDateInscriptions !== '') {
            if (informationModal.startDate > newElection.startDateInscriptions) {
              if (newElection.startDateVote !== '') {
                if (informationModal.endDate < newElection.startDateVote) {
                  setNewElection(aux);
                  setOpen(false);
                } else {
                  setMessage('La fecha de fin debe ser menor a la de la etapa de Votación');
                  setSeverity('error');
                  setOpenMessage(true);
                }
              } else {
                setNewElection(aux);
                setOpen(false);
              }
            } else {
              setMessage('La fecha de inicio debe ser mayor a la de la etapa de Inscripción');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else if (newElection.startDateVote !== '') {
            if (informationModal.endDate < newElection.startDateVote) {
              setNewElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de fin debe ser menor a la de la etapa de Votación');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            setNewElection(aux);
            setOpen(false);
          }
          break;
        case 'Vote':
          if (newElection.endDateActualization !== '') {
            if (informationModal.startDate > newElection.endDateActualization) {
              setNewElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de inicio debe ser mayor a la de la etapa de Actualización');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else if (newElection.startDateInscriptions !== '') {
            if (informationModal.startDate > newElection.endDateInscriptions) {
              setNewElection(aux);
            } else {
              setMessage('La fecha de inicio debe ser mayor a la de la etapa de Inscripción');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            setNewElection(aux);
            setOpen(false);
          }
          break;
        default:
          break;
      }
    } else {
      setMessage('La fecha de inicio debe ser menor a la fecha de fin');
      setSeverity('error');
      setOpenMessage(true);
    }
  };

  useEffect(() => {
    setLoading(false);
  }, []);

  function pad(number) {
    if (number < 10) {
      return `0${number}`;
    }
    return number;
  }

  function convertToFormatBackend(dateTime) {
    return (
      `${dateTime.getFullYear()
      }-${
        pad(dateTime.getMonth() + 1)
      }-${
        pad(dateTime.getDate())
      } ${pad(dateTime.getHours())
      }:${pad(dateTime.getMinutes())
      }:${pad(dateTime.getMilliseconds())
      }`
    );
  }

  const changeStartDate = (date) => {
    setDateStart(date);
    const infAux = { ...informationModal };
    infAux.startDate = convertToFormatBackend(date);
    setInformationModal(infAux);
  };

  const changeEndDate = (date) => {
    setDateEnd(date);
    const infAux = { ...informationModal };
    infAux.endDate = convertToFormatBackend(date);
    setInformationModal(infAux);
  };

  return (
    <Grid {...props}>
      <Box>
        <Box sx={{ m: 3 }}>
          <Typography variant="h1">Nueva Elección</Typography>
        </Box>
      </Box>
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h3" className={classes.titleText}>
            Nombre
          </Typography>
        </Grid>
        <Grid container direction="row" item xs={6} justify="center">
          <TextField
            fullWidth
            value={newElection.title}
            placeholder="Nombre"
            onChange={(event) => {
              const infAux = { ...newElection };
              infAux.name = event.target.value;
              setNewElection(infAux);
            }}
            variant="outlined"
            type="text"
            size="small"
          />
        </Grid>
      </Grid>

      <Grid item container style={{ margin: '3vh' }}>
        <Grid item container style={{ alignContent: 'center' }}>
          <Typography variant="h3" className={classes.titleText}>
            Descripcion
          </Typography>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }} xs={10}>
        <Grid container direction="row" item justify="center">
          <TextField
            fullWidth
            value={newElection.description}
            placeholder="Descripción"
            onChange={(event) => {
              const infAux = { ...newElection };
              infAux.description = event.target.value;
              setNewElection(infAux);
            }}
            variant="outlined"
            type="text"
            size="small"
            multiline
            rows={4}
          />
        </Grid>
      </Grid>
      <Box>
        <Box sx={{ m: 3 }}>
          <Typography variant="h3">Etapas</Typography>
        </Box>
      </Box>
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h4" className={classes.titleText}>
            Inscripción Listas
          </Typography>
        </Grid>

        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          onClick={() => { openModal('Inscriptions'); }}
        >
          <Grid
            item
            className={classes.arrow}
          >
            <ArrowForwardIos color="secondary" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h4" className={classes.titleText}>
            Actualización Candidatos
          </Typography>
        </Grid>

        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          onClick={() => { openModal('Actualization'); }}
        >
          <Grid
            item
            className={classes.arrow}
          >
            <ArrowForwardIos color="secondary" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h4" className={classes.titleText}>
            Votación
          </Typography>
        </Grid>

        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          onClick={() => { openModal('Vote'); }}
        >
          <Grid
            item
            className={classes.arrow}
          >
            <ArrowForwardIos color="secondary" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }} direction="row" alignContent="center" justifyContent="center">
        <Grid
          xs={4}
          item
          container
          style={{ margin: '2vw' }}
        >
          <Button
            color="secondary"
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            onClick={() => {
              createElection();
            }}
          >
            Crear Elección
          </Button>
        </Grid>
        <Grid
          xs={4}
          item
          container
          style={{ margin: '2vw' }}
        >
          <Button
            color="inherit"
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            onClick={() => {
              navigate('/admin/elections', { replace: true });
            }}
          >
            Cancelar
          </Button>
        </Grid>
      </Grid>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Grid container className={classes.paper}>
            <Grid item container style={{ margin: '1vh' }}>
              <Typography variant="h4" className={classes.titleText}>
                {`Detalles ${stage}`}
              </Typography>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid
                xs={4}
                item
                container
                style={{ alignContent: 'center' }}
              >
                <Typography variant="h6" className={classes.titleText}>
                  Inicio
                </Typography>
              </Grid>
              <Grid container direction="row" item xs={8} justify="center">
                <DatePickerStage
                  label="Fecha de inicio"
                  selectedDate={dateStart}
                  handleDateChange={changeStartDate}
                  minDate={new Date()}
                />
              </Grid>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid
                xs={4}
                item
                container
                style={{ alignContent: 'center' }}
              >
                <Typography variant="h6" className={classes.titleText}>
                  Fin
                </Typography>
              </Grid>
              <Grid container direction="row" item xs={8} justify="center">
                <DatePickerStage
                  label="Fecha de Fin"
                  selectedDate={dateEnd}
                  handleDateChange={changeEndDate}
                  minDate={dateStart}
                />
              </Grid>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid item container style={{ alignContent: 'center' }}>
                <Typography variant="h6" className={classes.titleText}>
                  Descripcion
                </Typography>
              </Grid>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid container direction="row" item justify="center">
                <TextField
                  fullWidth
                  value={informationModal.description}
                  placeholder="Descripción"
                  onChange={(event) => {
                    const aux = { ...informationModal };
                    aux.description = event.target.value;
                    setInformationModal(aux);
                  }}
                  variant="outlined"
                  type="text"
                  size="small"
                  multiline
                  rows={4}
                />
              </Grid>
            </Grid>
            <Grid
              container
              item
              xs={12}
              sm={4}
              justify="center"
              style={{ paddingBottom: '3vh', paddingTop: '3vh' }}
            >
              <Button
                border={15}
                variant="contained"
                fullWidth
                size="large"
                color="secondary"
                className={classes.button}
                onClick={saveInformation}
              >
                Confirmar Cambios
              </Button>
            </Grid>
          </Grid>
        </Fade>
      </Modal>
      <SnackbarComponent
        message={message}
        handleClose={handleCloseMessage}
        open={openMessage}
        severity={severity}
      />
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Grid>
  );
};

export default NewElection;

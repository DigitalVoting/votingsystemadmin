/* eslint-disable no-restricted-syntax */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Avatar,
  Button,
  Card,
  CardHeader,
  Grid,
  Paper,
  Typography,
} from '@material-ui/core';
import { useLocation, useNavigate } from 'react-router';
import {
  Chart,
  PieSeries,
  Legend,
} from '@devexpress/dx-react-chart-bootstrap4';
import '@devexpress/dx-react-chart-bootstrap4/dist/dx-react-chart-bootstrap4.css';
import electionsService from 'src/services/Elections/elections-service';
import { Animation } from '@devexpress/dx-react-chart';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: '100vw',
  },
  titleSection: {
    marginTop: theme.spacing(2),
  },
  titleText: {
    fontWeight: 'bold',
  },
  card: {
    border: '1px solid',
    borderColor: theme.palette.secondary.main,
    borderRadius: '12px',
    width: '100%',
  },
  header: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  button: {},
  iconSearch: {
    color: '#3E5481',
    cursor: 'pointer',
  },
  searchField: {
    borderRadius: 24,
    background: '#F4F5F7',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  lista: {
    width: '80px !important',
    height: '80px !important',
  },
}));

const Results = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();
  const [election, setElection] = useState({});
  const [winner, setWinner] = useState({});
  const [results, setResults] = useState([]);
  const [data, setData] = useState([]);
  const [detalle, setDetalle] = useState(false);
  const [published, setPublished] = useState(false);

  useEffect(() => {
    if (
      location.state
      && location.state.election
    ) {
      setElection(location.state.election);
      electionsService
        .getResults(location.state.election.id)
        .then((response) => {
          if (response.data[0]) {
            setPublished(true);
            setResults(response.data);
            const d = [];
            let win = { numberVotes: 0 };
            for (const result of response.data) {
              d.push({
                value: result.numberVotes,
                name: result.candidatesList.name,
                title: result.candidatesList.name,
              });
              if (result.numberVotes > win.numberVotes) win = result;
            }
            setWinner(win);
            setData(d);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      navigate('/admin/elections/details');
    }
  }, []);

  const VerDetalle = () => {
    setDetalle(!detalle);
  };

  const options = results.map((result) => (
    <Grid container sx={{ p: 3, pr: 0 }} key={result.id}>
      <Card className={classes.card}>
        <CardHeader
          disableTypography
          title={(
            <Grid
              item
              container
              direction="row"
              align="center"
              className={classes.title}
            >
              <Grid item xs={4} md={2} container justify="flex-start">
                <Avatar
                  alt={result.candidatesList.name}
                  src={result.candidatesList.logo}
                  className={classes.lista}
                />
              </Grid>
              <Grid
                item
                xs={8}
                container
                direction="column"
                justify="center"
                align="center"
              >
                <Grid item>
                  <Typography
                    variant="h4"
                    align="left"
                    style={{ alignItems: 'center' }}
                  >
                    {result.candidatesList.name}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                xs={12}
                container
                direction="column"
                justify="center"
                align="center"
              >
                <Grid item>
                  <Typography variant="body1" align="right">
                    {`Votos Obtenidos: ${result.numberVotes}`}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            )}
          className={classes.header}
        />
      </Card>
    </Grid>
  ));

  const labelMap = ({ text }) => <Typography variant="subtitle2">{text}</Typography>;
  return (
    <Grid container justify="center" sx={{ p: 3 }}>
      {published ? (
        <Grid
          container
          direction="column"
          justify="center"
          align="center"
          className={classes.root}
          spacing={3}
        >
          <Grid item>
            <Typography variant="h2" textAlign="left">{election.name}</Typography>
          </Grid>

          <Grid item container justify="flex-start">
            <Typography variant="h3" className={classes.titleText}>
              Resultados
            </Typography>
          </Grid>

          <Grid item container justify="flex-start">
            <Typography variant="h4" className={classes.titleText}>
              Ganador
            </Typography>
          </Grid>
          {winner && winner.candidatesList ? (
            <Grid container sx={{ p: 3 }}>
              <Grid container item xs={12} md={5} style={{ padding: '0' }}>
                <Card className={classes.card}>
                  <CardHeader
                    disableTypography
                    title={(
                      <Grid
                        item
                        container
                        direction="row"
                        align="center"
                        className={classes.title}
                      >
                        <Grid item xs={4} md={2} container justify="flex-start">
                          <Avatar
                            alt={winner.candidatesList.name}
                            src={winner.candidatesList.logo}
                            className={classes.lista}
                          />
                        </Grid>
                        <Grid
                          item
                          xs={8}
                          container
                          direction="column"
                          justify="center"
                          align="center"
                        >
                          <Grid container item style={{ height: '100%', alignContent: 'center' }}>
                            <Typography
                              variant="body1"
                              align="left"
                              style={{ alignItems: 'center' }}
                            >
                              {winner.candidatesList.name}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    )}
                    className={classes.header}
                  />
                </Card>
              </Grid>
            </Grid>
          ) : (
            <Grid />
          )}
          <Grid item>
            <Paper>
              <Chart data={data}>
                <PieSeries
                  valueField="value"
                  argumentField="title"
                  innerRadius={0.6}
                />
                <Animation />
                <Legend
                  title="Listas de Candidatos"
                  orientation="horizontal"
                  itemTextPosition="right"
                  horizontalAlignment="center"
                  columnCount={3}
                  position="left"
                  labelComponent={labelMap}
                />
              </Chart>
            </Paper>
          </Grid>

          <Grid item>
            <Button
              border={15}
              variant="contained"
              fullWidth
              size="large"
              color="primary"
              className={classes.button}
              onClick={VerDetalle}
            >
              {detalle ? 'Ocultar Detalle' : 'Ver Detalle'}
            </Button>
          </Grid>

          {options[0] && detalle ? options : <Grid />}
        </Grid>
      ) : (
        <Grid
          container
          direction="column"
          justify="center"
          align="center"
          className={classes.root}
          spacing={3}
        >
          <Grid item>
            <Typography>{election.title}</Typography>
          </Grid>

          <Grid item container justify="center">
            <Typography
              variant="h4"
              className={classes.titleText}
              align="center"
            >
              Resultados Pendientes de Publicación
            </Typography>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};
export default Results;

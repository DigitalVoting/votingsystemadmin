import React from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Container,
  Grid,
  TextField,
  Typography
} from '@material-ui/core';
import Snackbar from 'src/components/Snackbars/Snackbar';
import Logo from '../components/Logo';
import { AuthContext } from '../context/auth-context';
import authService from '../services/Authentication/auth.service';

const Login = () => {
  const navigate = useNavigate();
  const authContext = React.useContext(AuthContext);

  const [message, setMessage] = React.useState('');
  const [open, setOpen] = React.useState(false);

  const login = (values) => {
    authService
      .signIn(values.username, values.password)
      .then((response) => {
        if (response.id) {
          if (response.roles.includes('ROLE_ADMIN')) {
            console.log('Admin');
            authContext.login();
            navigate('/admin/home', { replace: true });
          } else {
            console.log('No tiene permisos de administrador');
            setMessage('No tiene permisos de administrador');
            setOpen(true);
          }
        } else {
          console.log('Credenciales Incorrectas');
          setMessage('Credenciales Incorrectas');
          setOpen(true);
        }
      })
      .catch((e) => {
        console.log(e);
        setMessage('Credenciales Incorrectas');
        setOpen(true);
      });
  };

  function handleClose() {
    setOpen(false);
  }

  return (
    <>

      <Helmet>
        <title>Login | Voting System</title>
      </Helmet>
      <Grid
        container
        direction="row"
        sx={{
          height: '100%'
        }}
      >
        <Grid
          item
          direction="column"
          xs={0}
          sm={6}
          sx={{
            backgroundColor: 'primary.main',
            display: 'flex',
            height: '100%',
            justifyContent: 'center'
          }}
        >
          <Grid />
        </Grid>
        <Grid
          item
          direction="column"
          xs={12}
          sm={6}
          alignItems="center"
          sx={{
            backgroundColor: 'background.default',
            display: 'flex',
            height: '100%',
            justifyContent: 'center'
          }}
        >

          <Container>
            <Formik
              initialValues={{
                username: '',
                password: ''
              }}
              validationSchema={Yup.object().shape({
                username: Yup.string().required('El usuario es requerido'),
                password: Yup.string().max(255).required('La contraseña es requerida')
              })}
              onSubmit={() => {
                navigate('/admin/home', { replace: true });
              }}
            >
              {({
                errors,
                handleBlur,
                handleChange,
                isSubmitting,
                touched,
                values
              }) => (
                <Grid>
                  <Logo />
                  <Box sx={{ mb: 3 }}>
                    <Typography
                      color="textPrimary"
                      variant="h2"
                      align="center"
                    >
                      Bienvenido
                    </Typography>
                    <Typography
                      color="textSecondary"
                      align="center"
                      gutterBottom
                      variant="body2"
                    >
                      Por favor, ingrese sus credenciales a continuación
                    </Typography>
                  </Box>
                  <TextField
                    error={Boolean(touched.username && errors.username)}
                    fullWidth
                    helperText={touched.username && errors.username}
                    label="Usuario"
                    margin="normal"
                    name="username"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="text"
                    value={values.username}
                    variant="outlined"
                  />
                  <TextField
                    error={Boolean(touched.password && errors.password)}
                    fullWidth
                    helperText={touched.password && errors.password}
                    label="Contraseña"
                    margin="normal"
                    name="password"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    type="password"
                    value={values.password}
                    variant="outlined"
                  />
                  <RouterLink
                    color="textSecondary"
                    variant="body1"
                    to="/login"
                    component="h6"
                    align="right"
                  >
                    <Typography
                      color="textPrimary"
                      variant="h6"
                      align="right"
                    >
                      ¿Olvidaste tu contraseña?
                    </Typography>
                  </RouterLink>
                  <Box sx={{ py: 2 }}>
                    <Button
                      color="secondary"
                      disabled={isSubmitting}
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                      onClick={() => {
                        login(values);
                      }}
                    >
                      Ingresar
                    </Button>
                  </Box>
                </Grid>
              )}
            </Formik>
          </Container>
        </Grid>
      </Grid>
      <Snackbar
        message={message}
        handleClose={handleClose}
        open={open}
        severity="error"
      />
    </>
  );
};

export default Login;

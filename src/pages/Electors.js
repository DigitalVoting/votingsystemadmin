/* eslint-disable no-restricted-syntax */
import {
  Box, Button, CircularProgress, DialogActions, Fade, Grid, IconButton, Modal, Snackbar, TextField, Typography,
} from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import { Helmet } from 'react-helmet';
import { makeStyles } from '@material-ui/styles';
import React, { useEffect } from 'react';
import ListPage from 'src/components/Lists/ListPage';
import ButtonFile from 'src/components/Buttons/ButtonFile';
import theme from 'src/theme';
import XLSX from 'xlsx';
import MassiveChargeTable from 'src/components/Tables/MassiveChargeTable';
import { Close } from '@material-ui/icons';
import usersService from '../services/Users/users.service';
import authService from '../services/Authentication/auth.service';

const attributes = ['names', 'lastNames'];

const useStyles = makeStyles(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '10px',
    maxWidth: '90vw',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
    zIndex: 2
  },
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
}));

const headImportCells = [{
  id: 1,
  name: 'Nombres',
  value: 'names',
},
{
  id: 2,
  name: 'Apellidos',
  value: 'lastNames',
},
{
  id: 3,
  name: 'Usuario',
  value: 'username',
},
{
  id: 4,
  name: 'Correo',
  value: 'email',
}
];

const Electors = (props) => {
  const classes = useStyles();

  const [informationModal, setInformationModal] = React.useState({
    id: 0,
    username: '',
    names: '',
    lastNames: '',
  });
  const [type, setType] = React.useState('new');
  const [electors, setElectors] = React.useState([]);
  const [inactiveElectors, setInactiveElectors] = React.useState([]);

  const user = JSON.parse(localStorage.getItem('user'));

  const [pages, setPages] = React.useState([]);
  const [open, setOpen] = React.useState(false);
  const [openMessage, setOpenMessage] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const [loadingActive, setLoadingActive] = React.useState(true);
  const [loadingInactive, setLoadingInactive] = React.useState(true);

  const addElector = () => {
    setType('new');
    setInformationModal({
      id: 0,
      username: '',
      names: '',
      lastNames: '',
    });
    setOpen(true);
  };

  const massiveCharge = () => {
    setPages([]);
    setType('import');
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseMessage = () => {
    setOpenMessage(false);
  };

  const saveUser = () => {
    setLoadingActive(true);
    authService
      .register(
        informationModal.username,
        informationModal.names,
        informationModal.lastNames,
        informationModal.email,
        user.id, ['elector'],
        `${informationModal.lastNames}-${informationModal.names}`
      )
      .then((response) => {
        console.log(response.data);
        const electorsAux = electors;
        electorsAux.push({ id: electors.length + 1, ...informationModal, photo: '' });
        setElectors(electorsAux);
        handleClose();
        setLoadingActive(false);
        setMessage('El usuario se registro correctamente');
        setOpenMessage(true);
      })
      .catch((e) => {
        setMessage('Estamos teniendo problemas con el servidor. Vuelva a intentarlo');
        setOpenMessage(true);
        console.log(e);
      });
  };

  const findActiveUsers = (find) => {
    setLoadingActive(true);
    usersService
      .findActiveUsers(find)
      .then((response) => {
        setElectors(response.data);
        setLoadingActive(false);
      })
      .catch((e) => {
        setMessage('Estamos teniendo problemas con el servidor. Vuelva a intentarlo');
        setOpenMessage(true);
        console.log(e);
        setLoadingActive(false);
      });
  };

  const findInactiveUsers = (find) => {
    setLoadingInactive(true);
    usersService
      .getInactiveUsers(find)
      .then((response) => {
        setInactiveElectors(response.data);
        setLoadingInactive(false);
      })
      .catch((e) => {
        setMessage('Estamos teniendo problemas con el servidor. Vuelva a intentarlo');
        setOpenMessage(true);
        console.log(e);
        setLoadingInactive(false);
      });
  };

  const handleSave = () => {
    const usersData = pages[0].data;
    console.log(usersData);
    usersService.saveElectorsList(
      {
        users: usersData,
        creationUser: user.id,
        role: 'elector'
      }
    ).then((response) => {
      const messageText = `Se registraron ${response.data.usersRegistered.length} usuarios y no se registraron ${response.data.usersNotRegistered.length}`;
      const usersAux = [...electors];
      for (const newUser of response.data.usersRegistered) {
        usersAux.push(newUser);
      }
      setElectors(usersAux);
      setMessage(messageText);
      handleClose();
      setOpenMessage(true);
    }).catch((e) => {
      console.log(e);
      setMessage('No se registraron los usuarios. Vuelva a intentarlo');
      setOpenMessage(true);
    });
  };

  const electorDetails = (elector) => {
    setType('new');
    setInformationModal(elector);
    setOpen(true);
  };

  useEffect(() => {
    findActiveUsers('');
    findInactiveUsers('');
  }, []);

  const handleInputChange = (event) => {
    const { target } = event;
    const hojas = [];
    console.log(target.type, 'archivo seleccionando');
    if (target.type === 'file') {
      const reader = new FileReader();
      reader.readAsArrayBuffer(target.files[0]);
      reader.onloadend = (e) => {
        const dataEx = new Uint8Array(e.target.result);
        const workbook = XLSX.read(dataEx, { type: 'array' });
        workbook.SheetNames.forEach((sheetName) => {
          const xlRowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          hojas.push({
            data: xlRowObject,
            sheetName
          });
        });
        console.log(hojas);
        setPages(hojas);
      };
    }
  };

  return (
    <>
      <Helmet>
        <title>Electores | Voting System</title>
      </Helmet>
      <Grid>
        <Box {...props}>
          <ListPage
            title="Electores"
            activeElements={electors}
            inactiveElements={inactiveElectors}
            descriptionAttributes={attributes}
            elementClickFunction={electorDetails}
            elementImg="photo"
            buttonFunction={addElector}
            buttonTitle="Añadir"
            secondButton
            secondButtonFunction={massiveCharge}
            findActives={findActiveUsers}
            findInactives={findInactiveUsers}
          />
        </Box>
        <Modal
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            {type === 'new'
              ? (
                <Grid container className={classes.paper}>
                  <Grid item container style={{ margin: '1vh' }}>
                    <Grid
                      xs={4}
                      item
                      container
                      style={{ alignContent: 'center' }}
                    >
                      <Typography variant="h6" className={classes.titleText}>
                        Usuario
                      </Typography>
                    </Grid>
                    <Grid container direction="row" item xs={8} justify="center">
                      <TextField
                        fullWidth
                        value={informationModal.username}
                        placeholder="Usuario"
                        onChange={(event) => {
                          const infAux = { ...informationModal };
                          infAux.username = event.target.value;
                          setInformationModal(infAux);
                        }}
                        variant="outlined"
                        type="text"
                        size="small"
                      />
                    </Grid>
                  </Grid>
                  <Grid item container style={{ margin: '1vh' }}>
                    <Grid
                      xs={4}
                      item
                      container
                      style={{ alignContent: 'center' }}
                    >
                      <Typography variant="h6" className={classes.titleText}>
                        Nombres
                      </Typography>
                    </Grid>
                    <Grid container direction="row" item xs={8} justify="center">
                      <TextField
                        fullWidth
                        value={informationModal.names}
                        placeholder="Nombres"
                        onChange={(event) => {
                          const infAux = { ...informationModal };
                          infAux.names = event.target.value;
                          setInformationModal(infAux);
                        }}
                        variant="outlined"
                        type="text"
                        size="small"
                      />
                    </Grid>
                  </Grid>
                  <Grid item container style={{ margin: '1vh' }}>
                    <Grid
                      xs={4}
                      item
                      container
                      style={{ alignContent: 'center' }}
                    >
                      <Typography variant="h6" className={classes.titleText}>
                        Apellidos
                      </Typography>
                    </Grid>
                    <Grid container direction="row" item xs={8} justify="center">
                      <TextField
                        fullWidth
                        value={informationModal.lastNames}
                        placeholder="Apellidos"
                        onChange={(event) => {
                          const infAux = { ...informationModal };
                          infAux.lastNames = event.target.value;
                          setInformationModal(infAux);
                        }}
                        variant="outlined"
                        type="text"
                        size="small"
                      />
                    </Grid>
                  </Grid>
                  <Grid item container style={{ margin: '1vh' }}>
                    <Grid
                      xs={4}
                      item
                      container
                      style={{ alignContent: 'center' }}
                    >
                      <Typography variant="h6" className={classes.titleText}>
                        Correo
                      </Typography>
                    </Grid>
                    <Grid container direction="row" item xs={8} justify="center">
                      <TextField
                        fullWidth
                        value={informationModal.email}
                        placeholder="Correo"
                        onChange={(event) => {
                          const infAux = { ...informationModal };
                          infAux.email = event.target.value;
                          setInformationModal(infAux);
                        }}
                        variant="outlined"
                        type="text"
                        size="small"
                      />
                    </Grid>
                  </Grid>
                  {informationModal.id === 0
                    ? (
                      <Grid
                        container
                        item
                        direction="row"
                        style={{ padding: '3vh' }}
                        justifyContent="center"
                      >
                        <Grid
                          item
                          xs={12}
                          sm={4}
                        >
                          <Button
                            border={15}
                            variant="contained"
                            fullWidth
                            size="large"
                            color="secondary"
                            className={classes.button}
                            onClick={saveUser}
                          >
                            Agregar Elector
                          </Button>
                        </Grid>
                      </Grid>
                    )
                    : (
                      <Grid />
                    )}
                </Grid>
              )
              : (
                <Grid container className={classes.paper} direction="column" align="center" justify="center">
                  <Grid container item>
                    <Grid container item direction="column" justifyContent="center" xs={6} style={{ alignContent: 'flex-start' }}>
                      <Typography>Importar archivo con los usuarios a añadir</Typography>
                    </Grid>
                    <Grid item xs={6} style={{ display: 'flex', justifyContent: 'flex-end' }}>
                      <ButtonFile
                        handleInputChange={handleInputChange}
                      />
                    </Grid>
                  </Grid>
                  {pages.length > 0
                    ? (
                      <Grid item xs={12}>
                        <MassiveChargeTable dataTable={pages[0].data} headCells={headImportCells} />
                      </Grid>
                    )
                    : <Grid />}
                  <DialogActions style={{ marginTop: '2vh' }}>
                    <Button onClick={handleClose} color="primary">
                      Cerrar
                    </Button>
                    <Button onClick={handleSave} color="primary">
                      Guardar
                    </Button>
                  </DialogActions>
                </Grid>
              )}
          </Fade>
        </Modal>
        <Backdrop className={classes.backdrop} open={loadingActive || loadingInactive}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          message={message}
          handleClose={handleClose}
          open={openMessage}
          severity="error"
          action={(
            <>
              <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseMessage}>
                <Close fontSize="small" />
              </IconButton>
            </>
          )}
        />
      </Grid>
    </>
  );
};

export default Electors;

import { Helmet } from 'react-helmet';
import { Backdrop, Box, CircularProgress } from '@material-ui/core';
import ListPage from 'src/components/Lists/ListPage';
import { useNavigate } from 'react-router';
import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import candidatesListService from '../services/Candidates/candidate-list.service';

const attributes = ['name'];

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
}));

const CandidatesLists = (props) => {
  const navigate = useNavigate();
  const classes = useStyles();

  const [candidatesLists, setCandidatesLists] = React.useState([]);
  const [inactiveCandidatesLists, setInactiveCandidatesLists] = React.useState([]);
  const [loadingActive, setLoadingActive] = React.useState(true);
  const [loadingInactive, setLoadingInactive] = React.useState(true);

  const goSolicitudes = () => {
    navigate('/admin/solicitudes', { replace: true });
  };

  const goToDetails = (candidatesList) => {
    if (candidatesList.id) {
      navigate('/admin/candidates-lists/details', {
        replace: true,
        state: { candidatesList, type: 'view' },
      });
    }
  };

  const findCandidatesLists = (find) => {
    setLoadingActive(true);
    candidatesListService
      .findCandidatesListsCurrent(find)
      .then((response) => {
        setCandidatesLists(response.data);
        setLoadingActive(false);
      })
      .catch((e) => { console.log(e); });
  };

  const findInactiveCandidatesLists = (find) => {
    setLoadingInactive(true);
    candidatesListService
      .findInactiveCandidatesLists(find)
      .then((response) => {
        setInactiveCandidatesLists(response.data);
        setLoadingInactive(false);
      })
      .catch((e) => { console.log(e); });
  };

  useEffect(() => {
    candidatesListService
      .getCandidatesListsCurrent()
      .then((response) => {
        setCandidatesLists(response.data);
        setLoadingActive(false);
      })
      .catch((e) => { console.log(e); });
    findInactiveCandidatesLists('');
  }, []);

  return (
    <>
      <Helmet>
        <title>Listas de Candidatos | Voting System</title>
      </Helmet>
      <Box {...props}>
        <ListPage
          title="Listas de Candidatos"
          activeElements={candidatesLists}
          inactiveElements={inactiveCandidatesLists}
          elementClickFunction={goToDetails}
          descriptionAttributes={attributes}
          elementImg="logo"
          buttonFunction={goSolicitudes}
          buttonTitle="Ver Solicitudes"
          secondButton={false}
          findActives={findCandidatesLists}
          findInactives={findInactiveCandidatesLists}
        />
      </Box>
      <Backdrop className={classes.backdrop} open={loadingActive || loadingInactive}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};

export default CandidatesLists;

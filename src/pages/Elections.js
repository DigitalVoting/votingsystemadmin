import {
  Box,
  TextField,
  InputAdornment,
  SvgIcon,
  Grid,
  Typography,
  Backdrop,
  CircularProgress,
  Button,
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import { Helmet } from 'react-helmet';
import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import ElectionCard from 'src/components/Lists/ElectionCard';
import { useNavigate } from 'react-router';
import electionsService from '../services/Elections/elections-service';

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
}));

const onKeyUp = (event, findActives, findInactives, findText) => {
  if (event.charCode === 13) {
    findActives(findText);
    findInactives(findText);
  }
};

const Elections = (props) => {
  const classes = useStyles();
  const navigate = useNavigate();

  const [activeElections, setActiveElections] = useState([]);
  const [inactiveElections, setInactiveElections] = useState([]);

  const [searchText, setSearchText] = useState('');

  const [loadingActive, setLoadingActive] = useState(true);
  const [loadingInactive, setLoadingInactive] = useState(true);

  const findActiveElections = (find) => {
    setLoadingActive(true);
    electionsService
      .getActiveElections(find)
      .then((response) => {
        setActiveElections(response.data);
        setLoadingActive(false);
      })
      .catch((e) => { console.log(e); });
  };

  const findInactiveElections = (find) => {
    setLoadingInactive(true);
    electionsService
      .getInactiveElections(find)
      .then((response) => {
        setInactiveElections(response.data);
        setLoadingInactive(false);
      })
      .catch((e) => { console.log(e); });
  };

  const newElection = () => {
    navigate('/admin/elections/new', { replace: true });
  };

  useEffect(() => {
    findActiveElections('');
    findInactiveElections('');
  }, []);

  const goElectionDetails = (election) => {
    if (election.id) {
      navigate('/admin/elections/details', {
        replace: true,
        state: { election },
      });
    }
  };

  const mapElections = (elements) => elements.map((element) => (
    <ElectionCard
      key={element.id}
      element={element}
      imgVar={element.banner ? 'banner' : 'logo'}
      title={element.title ? 'title' : 'name'}
      xs={3}
      md={3}
      clickFunction={goElectionDetails}
    />
  ));

  return (
    <>
      <Helmet>
        <title>Elecciones | Voting System</title>
      </Helmet>
      <Grid>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            mt: 2,
            mr: 2,
          }}
        >
          <Button
            color="secondary"
            variant="contained"
            onClick={() => { newElection(); }}
          >
            Crear Elección
          </Button>
        </Box>
        <Box {...props}>
          <Box sx={{ m: 3 }}>
            <Typography variant="h1">Elecciones</Typography>
          </Box>
          <Box sx={{ m: 3 }}>
            <Box sx={{ maxWidth: 500 }}>
              <TextField
                fullWidth
                value={searchText}
                onChange={(event) => {
                  setSearchText(event.target.value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        fontSize="small"
                        color="action"
                      >
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                placeholder="Buscar"
                variant="outlined"
                onKeyPress={(event) => { onKeyUp(event, findActiveElections, findInactiveElections, searchText); }}
              />
            </Box>
          </Box>
          <Grid container direction="column" sx={{ p: 3 }}>
            <Box>
              <Typography variant="h3">Elecciones Activas</Typography>
            </Box>
            <Grid container style={{ overflowX: 'auto', flexWrap: 'nowrap', scrollbarWidth: 'none' }}>
              {mapElections(activeElections)}
              {activeElections.length > 0
                ? (<Grid />)
                : (
                  <Typography variant="h3" m={5}>No se encontraron registros</Typography>
                )}
            </Grid>
          </Grid>
          <Grid container direction="column" sx={{ p: 3 }}>
            <Box>
              <Typography variant="h3">Elecciones Inactivas</Typography>
            </Box>
            <Grid container style={{ overflowX: 'auto', flexWrap: 'nowrap', scrollbarWidth: 'none' }}>
              {mapElections(inactiveElections)}
              {inactiveElections.length > 0
                ? (<Grid />)
                : (
                  <Typography variant="h3" m={5}>No se encontraron registros</Typography>
                )}
            </Grid>
          </Grid>
        </Box>
        <Backdrop className={classes.backdrop} open={loadingActive || loadingInactive}>
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    </>
  );
};

export default Elections;

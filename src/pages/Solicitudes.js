import { Helmet } from 'react-helmet';
import { Box, Container, Typography } from '@material-ui/core';
import CustomerListResults from 'src/components/customer/CustomerListResults';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import solicitudesService from '../services/Solicitudes/solicitudes-sevice';

const Solicitudes = () => {
  const navigate = useNavigate();
  const [solicitudes, setSolicitudes] = useState([]);

  const getSolicitudes = () => {
    solicitudesService
      .getSolicitudes()
      .then((response) => {
        setSolicitudes(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const answerSolicitude = (solicitude) => {
    if (solicitude.title === 'Actualización de Información') {
      navigate('/admin/candidates/details', {
        replace: true,
        state: {
          candidate: solicitude.user,
          solicitudeId: solicitude.id,
          type: 'sol',
        }
      });
    } else {
      navigate('/admin/candidates-lists/details', {
        replace: true,
        state: {
          candidatesList: solicitude.candidatesList,
          solicitudeId: solicitude.id,
          type: 'sol',
        }
      });
    }
  };

  useEffect(() => {
    getSolicitudes();
  }, []);

  return (
    <>
      <Helmet>
        <title>Solicitudes | Voting System</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Box sx={{ m: 3 }}>
          <Typography variant="h1">Solicitudes</Typography>
        </Box>
        {solicitudes.length > 0 ? (
          <Container maxWidth={false}>
            <Box sx={{ pt: 3 }}>
              <CustomerListResults customers={solicitudes} rowFunction={answerSolicitude} />
            </Box>
          </Container>
        )
          : (
            <Container maxWidth={false}>
              <Typography>No hay solicitudes Pendientes</Typography>
            </Container>
          )}
      </Box>
    </>
  );
};

export default Solicitudes;

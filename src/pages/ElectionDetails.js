import {
  Box,
  Grid,
  Typography,
  Backdrop,
  CircularProgress,
  TextField,
  Modal,
  Fade,
  Button,
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { ArrowForwardIos } from '@material-ui/icons';
import DatePickerStage from 'src/components/DatePickers/DatePickerStage';
import theme from 'src/theme';
import SnackbarComponent from 'src/components/Snackbars/Snackbar';
import { useNavigate, useLocation } from 'react-router';
import electionsService from 'src/services/Elections/elections-service';

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
  arrow: {
    cursor: 'pointer',
    backgroundColor: '#851610',
    color: 'FFFFFF',
    borderRadius: '10px',
  },
  arrowDisabled: {
    backgroundColor: 'gray',
    color: 'FFFFFF',
    borderRadius: '10px',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: '10px',
    maxWidth: '90vw',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
    zIndex: 2
  },
}));

const ElectionDetails = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();

  const user = JSON.parse(localStorage.getItem('user'));

  const [election, setElection] = useState({
    name: '',
    description: '',
    logo: 'none',
    status: 'ACT',
    descriptionInscriptions: '',
    startDateInscriptions: '',
    endDateInscriptions: '',
    descriptionActualization: '',
    startDateActualization: '',
    endDateActualization: '',
    descriptionVote: '',
    startDateVote: '',
    endDateVote: '',
    creationUser: user.id,
  });
  const [currentStage, setCurrentStage] = useState('');
  const [stage, setStage] = useState('');
  const [informationModal, setInformationModal] = useState({ startDate: new Date(), endDate: new Date(), description: '' });
  const [dateStart, setDateStart] = useState(new Date());
  const [dateEnd, setDateEnd] = useState(new Date());

  const [open, setOpen] = useState(false);
  const [active, setActive] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [message, setMessage] = useState('');
  const [severity, setSeverity] = useState('error');
  const [loading, setLoading] = useState(false);

  const parseDate = (date, functionSet) => {
    const year = parseInt(date.substr(0, 4), 10);
    const month = parseInt(date.substr(5, 7), 10) - 1;
    const day = parseInt(date.substr(8, 10), 10);
    const hour = parseInt(date.substr(11, 13), 10);
    const min = parseInt(date.substr(14, 16), 10);
    const sec = parseInt(date.substr(17, 19), 10);

    functionSet(new Date(year, month, day, hour, min, sec, 0));
  };

  const openModal = (stageSelected) => {
    console.log(stageSelected);

    const inf = {
      startDate: election[`startDate${stageSelected}`],
      endDate: election[`endDate${stageSelected}`],
      description: election[`description${stageSelected}`],
    };
    if (inf.startDate !== '') {
      parseDate(inf.startDate, setDateStart);
    }
    if (inf.endDate !== '') {
      parseDate(inf.endDate, setDateEnd);
    }

    switch (currentStage) {
      case 'inscriptions':
        if (stageSelected === 'Vote' || stageSelected === 'Actualization') setActive(true);
        else setActive(false);
        break;
      case 'actualization':
        if (stageSelected === 'Vote') setActive(true);
        else setActive(false);
        break;
      case 'vote':
        setActive(false);
        break;
      case 'waiting':
        setActive(true);
        break;
      default:
        setActive(false);
        break;
    }
    setInformationModal(inf);
    setStage(stageSelected);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseMessage = () => {
    setOpenMessage(false);
  };

  const createElection = () => {
    console.log(election);
    if (election.endDateInscriptions < election.startDateActualization && election.endDateActualization < election.startDateVote) {
      electionsService
        .updateElection(election)
        .then((response) => {
          if (response.data) {
            setMessage('Actualización de información realizada');
            setSeverity('success');
            setOpenMessage(true);
          } else {
            setMessage('Error en la operación. Vuelva a intentarlo');
            setSeverity('error');
            setOpenMessage(true);
          }
        })
        .catch((e) => {
          console.log(e);
          setMessage('Error en la operación. Vuelva a intentarlo');
          setSeverity('error');
          setOpenMessage(true);
        });
    } else {
      setMessage('Las fechas de las etapas deben coherentes. Realizar los cambios respectivos');
      setSeverity('error');
      setOpenMessage(true);
    }
  };

  const saveInformation = () => {
    if (informationModal.startDate < informationModal.endDate) {
      const aux = { ...election };

      aux[`startDate${stage}`] = informationModal.startDate;
      aux[`endDate${stage}`] = informationModal.endDate;
      aux[`description${stage}`] = informationModal.description;
      switch (stage) {
        case 'Inscriptions':
          if (election.startDateActualization !== '') {
            if (informationModal.endDate < election.startDateActualization) {
              setElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de fin debe ser menor a la de la etapa de Actualización');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else if (election.endDateVote !== '') {
            if (informationModal.endDate < election.endDateVote) {
              setElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de fin debe ser menor a la de la etapa de Votación');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            setElection(aux);
            setOpen(false);
          }
          break;
        case 'Actualization':
          if (election.endDateInscriptions !== '') {
            if (informationModal.startDate > election.startDateInscriptions) {
              if (election.startDateVote !== '') {
                if (informationModal.endDate < election.startDateVote) {
                  setElection(aux);
                  setOpen(false);
                } else {
                  setMessage('La fecha de fin debe ser menor a la de la etapa de Votación');
                  setSeverity('error');
                  setOpenMessage(true);
                }
              } else {
                setElection(aux);
                setOpen(false);
              }
            } else {
              setMessage('La fecha de inicio debe ser mayor a la de la etapa de Inscripción');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else if (election.startDateVote !== '') {
            if (informationModal.endDate < election.startDateVote) {
              setElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de fin debe ser menor a la de la etapa de Votación');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            setElection(aux);
            setOpen(false);
          }
          break;
        case 'Vote':
          if (election.endDateActualization !== '') {
            if (informationModal.startDate > election.endDateActualization) {
              setElection(aux);
              setOpen(false);
            } else {
              setMessage('La fecha de inicio debe ser mayor a la de la etapa de Actualización');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else if (election.startDateInscriptions !== '') {
            if (informationModal.startDate > election.endDateInscriptions) {
              setElection(aux);
            } else {
              setMessage('La fecha de inicio debe ser mayor a la de la etapa de Inscripción');
              setSeverity('error');
              setOpenMessage(true);
            }
          } else {
            setElection(aux);
            setOpen(false);
          }
          break;
        default:
          break;
      }
    } else {
      setMessage('La fecha de inicio debe ser menor a la fecha de fin');
      setSeverity('error');
      setOpenMessage(true);
    }
  };

  const getStage = (id) => {
    electionsService
      .getElectionStage(id)
      .then((response) => {
        setCurrentStage(response.data);
      })
      .catch((e) => {
        console.log(e);
        setMessage('Error en la operación. Vuelva a intentarlo');
        setSeverity('error');
        setOpenMessage(true);
      });
  };

  useEffect(() => {
    setLoading(false);
    if (location.state === null || location.state.election === null) {
      navigate('/admin/elections');
    } else {
      getStage(location.state.election.id);
      const aux = location.state.election;
      console.log(aux);
      aux.startDateInscriptions = (aux.startDateInscriptions.replace('T', ' '));
      aux.endDateInscriptions = (aux.endDateInscriptions.replace('T', ' '));
      aux.startDateActualization = (aux.startDateActualization.replace('T', ' '));
      aux.endDateActualization = (aux.endDateActualization.replace('T', ' '));
      aux.startDateVote = (aux.startDateVote.replace('T', ' '));
      aux.endDateVote = (aux.endDateVote.replace('T', ' '));
      aux.creationUser = user.id;
      setElection(aux);
    }
  }, []);

  function pad(number) {
    if (number < 10) {
      return `0${number}`;
    }
    return number;
  }

  function convertToFormatBackend(dateTime) {
    return (
      `${dateTime.getFullYear()
      }-${
        pad(dateTime.getMonth() + 1)
      }-${
        pad(dateTime.getDate())
      } ${pad(dateTime.getHours())
      }:${pad(dateTime.getMinutes())
      }:${pad(dateTime.getMilliseconds())
      }`
    );
  }

  const changeStartDate = (date) => {
    setDateStart(date);
    const infAux = { ...informationModal };
    infAux.startDate = convertToFormatBackend(date);
    setInformationModal(infAux);
  };

  const changeEndDate = (date) => {
    setDateEnd(date);
    const infAux = { ...informationModal };
    infAux.endDate = convertToFormatBackend(date);
    setInformationModal(infAux);
  };

  const getStageName = (stg) => {
    switch (stg) {
      case 'inscriptions':
        return 'Inscripciones';
      case 'actualization':
        return 'Actualización';
      case 'vote':
        return 'Votación';
      case 'waiting':
        return 'Pendiente';
      default:
        return 'Resultados';
    }
  };

  return (
    <Grid>
      <Box>
        <Box sx={{ m: 3 }}>
          <Typography variant="h1">{election.name}</Typography>
        </Box>
      </Box>

      <Grid item container style={{ margin: '3vh' }}>
        <Grid item container style={{ alignContent: 'center' }}>
          <Typography variant="h3" className={classes.titleText}>
            Descripcion
          </Typography>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }} xs={10}>
        <Grid container direction="row" item justify="center">
          <TextField
            fullWidth
            value={election.description}
            placeholder="Descripción"
            onChange={(event) => {
              const infAux = { ...election };
              infAux.description = event.target.value;
              setElection(infAux);
            }}
            variant="outlined"
            type="text"
            size="small"
            multiline
            rows={4}
          />
        </Grid>
      </Grid>
      <Box>
        <Box style={{ margin: '3vh' }}>
          <Typography variant="h3">Etapas</Typography>
        </Box>
      </Box>
      {currentStage !== '' && currentStage !== 'waiting' ? (
        <Grid item container style={{ margin: '3vh' }}>
          <Grid
            xs={4}
            item
            container
            style={{ alignContent: 'center' }}
          >
            <Typography variant="h4">{`Etapa Actual: ${getStageName(currentStage)}`}</Typography>
          </Grid>
        </Grid>
      )
        : (
          <Grid />
        )}
      {currentStage !== '' && currentStage === 'results' ? (
        <Grid item container xs={2} style={{ margin: '3vh' }}>
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={() => {
              navigate('/admin/elections/results', {
                replace: true,
                state: { election, stage }
              });
            }}
          >
            Ver Resultados
          </Button>
        </Grid>
      )
        : (
          <Grid />
        )}
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h4" className={classes.titleText}>
            Inscripción Listas
          </Typography>
        </Grid>

        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          onClick={() => { openModal('Inscriptions'); }}
        >
          <Grid
            item
            className={classes.arrow}
          >
            <ArrowForwardIos color="secondary" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h4" className={classes.titleText}>
            Actualización Candidatos
          </Typography>
        </Grid>

        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          onClick={() => { openModal('Actualization'); }}
        >
          <Grid
            item
            className={classes.arrow}
          >
            <ArrowForwardIos color="secondary" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }}>
        <Grid
          xs={4}
          item
          container
          style={{ alignContent: 'center' }}
        >
          <Typography variant="h4" className={classes.titleText}>
            Votación
          </Typography>
        </Grid>

        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          onClick={() => { openModal('Vote'); }}
        >
          <Grid
            item
            className={classes.arrow}
          >
            <ArrowForwardIos color="secondary" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item container style={{ margin: '3vh' }} direction="row" alignContent="center" justifyContent="center">
        <Grid
          xs={4}
          item
          container
          style={{ margin: '2vw' }}
        >
          <Button
            color="secondary"
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            onClick={() => {
              createElection();
            }}
          >
            Confirmar Cambios
          </Button>
        </Grid>
        <Grid
          xs={4}
          item
          container
          style={{ margin: '2vw' }}
        >
          <Button
            color="inherit"
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            onClick={() => {
              navigate('/admin/elections', { replace: true });
            }}
          >
            Cancelar
          </Button>
        </Grid>
      </Grid>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Grid container className={classes.paper}>
            <Grid item container style={{ margin: '1vh' }}>
              <Typography variant="h4" className={classes.titleText}>
                {`Detalles ${stage}`}
              </Typography>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid
                xs={4}
                item
                container
                style={{ alignContent: 'center' }}
              >
                <Typography variant="h6" className={classes.titleText}>
                  Inicio
                </Typography>
              </Grid>
              <Grid container direction="row" item xs={8} justify="center">
                <DatePickerStage
                  label="Fecha de inicio"
                  selectedDate={dateStart}
                  handleDateChange={changeStartDate}
                  minDate={new Date()}
                />
              </Grid>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid
                xs={4}
                item
                container
                style={{ alignContent: 'center' }}
              >
                <Typography variant="h6" className={classes.titleText}>
                  Fin
                </Typography>
              </Grid>
              <Grid container direction="row" item xs={8} justify="center">
                <DatePickerStage
                  label="Fecha de Fin"
                  selectedDate={dateEnd}
                  handleDateChange={changeEndDate}
                  minDate={dateStart}
                />
              </Grid>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid item container style={{ alignContent: 'center' }}>
                <Typography variant="h6" className={classes.titleText}>
                  Descripcion
                </Typography>
              </Grid>
            </Grid>
            <Grid item container style={{ margin: '1vh' }}>
              <Grid container direction="row" item justify="center">
                <TextField
                  fullWidth
                  value={informationModal.description}
                  placeholder="Descripción"
                  onChange={(event) => {
                    const aux = { ...informationModal };
                    aux.description = event.target.value;
                    setInformationModal(aux);
                  }}
                  variant="outlined"
                  type="text"
                  size="small"
                  multiline
                  rows={4}
                />
              </Grid>
            </Grid>
            {active ? (
              <Grid
                container
                item
                xs={12}
                sm={4}
                justify="center"
                style={{ paddingBottom: '3vh', paddingTop: '3vh' }}
              >
                <Button
                  border={15}
                  variant="contained"
                  fullWidth
                  size="large"
                  color="secondary"
                  className={classes.button}
                  onClick={saveInformation}
                >
                  Confirmar Cambios
                </Button>
              </Grid>
            )
              : (
                <Grid> </Grid>
              )}
          </Grid>
        </Fade>
      </Modal>
      <SnackbarComponent
        message={message}
        handleClose={handleCloseMessage}
        open={openMessage}
        severity={severity}
      />
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Grid>
  );
};
export default ElectionDetails;

import { Helmet } from 'react-helmet';
import {
  Avatar,
  Backdrop, Box, Button, CircularProgress, Grid, Typography
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useNavigate, useLocation } from 'react-router';
import { makeStyles } from '@material-ui/styles';
import getInitials from 'src/utils/getInitials';
import ElementCard from 'src/components/Lists/ElementCard';
import candidatesListService from '../services/Candidates/candidate-list.service';

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
}));

const GetDescription = (element, attributes) => {
  let description = '';
  // eslint-disable-next-line no-restricted-syntax
  for (const variable of attributes) {
    description = description.concat(element[variable]).concat(' ');
  }
  return description;
};

const mapElements = (elements, elementImg, elementClickFunction) => elements.map((element) => (
  <Grid>
    <Typography variant="h5" textAlign="center" style={{ marginTop: '15px' }}>{element.position}</Typography>
    <ElementCard
      key={element.id}
      element={element}
      description={GetDescription(element, ['names', 'lastNames'])}
      imgVar={elementImg}
      onClick={() => { elementClickFunction(element); }}
      xs={2}
      md={1}
    />
  </Grid>
));

const CandidatesListDetails = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();

  const user = JSON.parse(localStorage.getItem('user'));

  const [candidatesList, setCandidatesList] = useState(false);
  const [loading, setLoading] = useState(false);
  const [candidates, setCandidates] = useState([]);

  const getCandidates = (id) => {
    candidatesListService
      .getCandidatesOfCandidatesList(id)
      .then((response) => {
        setCandidates(response.data);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
  };

  useEffect(() => {
    setLoading(true);
    if (location.state === null || location.state.candidatesList === null || location.state === undefined || location.state.candidatesList === undefined) {
      navigate('/admin/candidates-lists');
    } else {
      setCandidatesList(location.state.candidatesList);
      getCandidates(location.state.candidatesList.id);
    }
  }, []);

  let proposalsContent = null;
  if (candidatesList) {
    proposalsContent = candidatesList.proposals.map((proposal) => (
      <Grid key={proposal.id} direction="column" item container className={classes.title}>
        <Typography variant="h3">
          {proposal.name}
        </Typography>
        <Typography
          variant="subtitle1"
          align="justify"
          className={classes.subtitleText}
        >
          {proposal.description}
        </Typography>
      </Grid>
    ));
  }

  const goToDetails = (candidate) => {
    navigate('/admin/candidates/details', {
      replace: true,
      state: { candidate, type: 'view' },
    });
  };

  const answerSolicitude = (answer) => {
    setLoading(true);
    if (answer) {
      candidatesListService
        .updateCandidateList(candidatesList.id, candidatesList.name, candidatesList.description, candidatesList.logo, 'ACT', user.id, 'answer', location.state.solicitudeId)
        .then((response) => {
          console.log(response.data);
          setLoading(false);
          navigate('/admin/candidates-lists/details');
        });
    } else {
      candidatesListService
        .updateCandidateList(candidatesList.id, candidatesList.name, candidatesList.description, candidatesList.logo, 'PEN', user.id, 'answer', location.state.solicitudeId)
        .then((response) => {
          console.log(response.data);
          setLoading(false);
          navigate('/admin/candidates-lists/details');
        });
    }
  };

  return (
    <>
      <Helmet>
        <title>Candidates List Details | Voting System</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Box sx={{
          m: 3,
          alignItems: 'center',
          display: 'flex'
        }}
        >
          <Avatar
            src={candidatesList.logo}
            sx={{ mr: 2 }}
          >
            {getInitials(candidatesList.name)}
          </Avatar>
          <Typography
            color="textPrimary"
            variant="h1"
          >
            {candidatesList.name}
          </Typography>
        </Box>
        <Box sx={{
          m: 3
        }}
        >
          <Typography
            color="textPrimary"
            variant="h2"
          >
            Descripción
          </Typography>
        </Box>
        <Box sx={{
          m: 3,
        }}
        >
          <Typography>{candidatesList.description}</Typography>
        </Box>
        <Grid container direction="column" sx={{ p: 3 }}>
          <Box>
            <Typography variant="h2">Candidatos</Typography>
          </Box>
          <Grid
            container
            style={{
              overflowX: 'auto', flexWrap: 'nowrap', scrollbarWidth: 'none', msOverflowStyle: 'none'
            }}
          >
            {mapElements(candidates, 'photo', goToDetails)}
            {candidates.length > 0
              ? (<Grid />)
              : (
                <Typography variant="h3" m={5}>No cuenta con candidatos</Typography>
              )}
          </Grid>
        </Grid>
        <Grid container direction="column" sx={{ p: 3 }}>
          <Box>
            <Typography variant="h2">Propuestas</Typography>
          </Box>
          <Grid
            container
            style={{ marginTop: '1vh' }}
          >
            {proposalsContent}
          </Grid>
        </Grid>
        {location.state && location.state.type === 'view' ? (
          <Grid item container style={{ margin: '3vh' }} direction="row" alignContent="center" justifyContent="center">
            <Grid
              xs={4}
              item
              container
              style={{ margin: '2vw' }}
            >
              <Button
                color="inherit"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={() => {
                  navigate('/admin/candidates-lists');
                }}
              >
                Volver
              </Button>
            </Grid>
          </Grid>
        ) : (
          <Grid item container style={{ margin: '3vh' }} direction="row" alignContent="center" justifyContent="center">
            <Grid
              xs={4}
              item
              container
              style={{ margin: '2vw' }}
            >
              <Button
                color="secondary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={() => { answerSolicitude(true); }}
              >
                Aceptar Lista
              </Button>
            </Grid>
            <Grid
              xs={4}
              item
              container
              style={{ margin: '2vw' }}
            >
              <Button
                color="inherit"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={() => { answerSolicitude(false); }}
              >
                Rechazar
              </Button>
            </Grid>
          </Grid>
        )}
      </Box>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};

export default CandidatesListDetails;

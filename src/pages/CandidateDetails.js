import { Helmet } from 'react-helmet';
import {
  Avatar,
  Backdrop, Box, Button, CircularProgress, Grid, Typography
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useNavigate, useLocation } from 'react-router';
import { makeStyles } from '@material-ui/styles';
import getInitials from 'src/utils/getInitials';
import informationService from '../services/Candidates/information.service';

const useStyles = makeStyles(() => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
  avatar: {
    width: '120px !important',
    height: '120px !important',
  },
  title: {
    marginTop: '2vh',
  }
}));

const CandidatesListDetails = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();

  const user = JSON.parse(localStorage.getItem('user'));

  const [candidate, setCandidate] = useState();
  const [otherProfile, setOtherProfile] = useState([]);
  const [profile, setProfile] = useState(true);
  const [loading, setLoading] = useState(false);
  const [presentation, setPresentation] = useState('');
  const [professionalInformation, setProfessionalInformation] = useState([]);
  const [educationInformation, setEducationInformation] = useState([]);

  const getInformation = (candidateLoc, status) => {
    informationService
      .getCandidateInformation(
        candidateLoc.id,
        candidateLoc.candidatesLists[0].id,
        status,
      )
      .then((response) => {
        if (response.data) {
          setProfessionalInformation(
            response.data.filter(
              (information) => information.type === 'professional experience'
            )
          );
          setEducationInformation(
            response.data.filter(
              (information) => information.type === 'education experience'
            )
          );

          setPresentation(
            response.data.find(
              (information) => information.type === 'description'
            )
          );
        }
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    console.log(location.state);
    if (location.state === null || location.state.candidate === null || location.state === undefined || location.state.candidate === undefined) {
      navigate('/admin/candidates-lists');
    } else {
      setCandidate(location.state.candidate);
      getInformation(location.state.candidate, 'actual');
      setLoading(true);
      setProfile(true);
    }
  }, []);

  const answerSolicitude = (answer) => {
    setLoading(true);
    console.log(candidate);
    console.log(user);
    if (answer) {
      informationService
        .acceptChanges(
          candidate.id,
          candidate.candidatesLists[candidate.candidatesLists.length - 1].id,
          user.id,
          'ACT',
          location.state.solicitudeId
        )
        .then((response) => {
          console.log(response.data);
          navigate('/admin/candidates/details');
          setLoading(false);
        })
        .catch((e) => {
          console.log(e);
          setLoading(false);
        });
    } else {
      informationService
        .acceptChanges(
          candidate.id,
          candidate.candidatesLists[candidate.candidatesLists.length - 1].id,
          user.id,
          'DEL',
          location.state.solicitudeId
        )
        .then((response) => {
          console.log(response.data);
          navigate('/admin/candidates/details');
          setLoading(false);
        })
        .catch((e) => {
          console.log(e);
          setLoading(false);
        });
    }
  };

  const changeProfile = () => {
    setLoading(true);
    if (otherProfile.length) {
      const auxPres = { ...presentation };
      const auxProf = [...professionalInformation];
      const auxEduc = [...educationInformation];

      setPresentation(otherProfile[0]);
      setProfessionalInformation(otherProfile[1]);
      setEducationInformation(otherProfile[2]);

      setOtherProfile([auxPres, auxProf, auxEduc]);
      setLoading(false);
    } else {
      const auxPres = { ...presentation };
      const auxProf = [...professionalInformation];
      const auxEduc = [...educationInformation];

      setOtherProfile([auxPres, auxProf, auxEduc]);

      getInformation(location.state.candidate, 'sol');
    }
    setProfile(!profile);
  };

  const professionalExperienceContent = professionalInformation.map(
    (information) => (
      <Grid key={information.id} item container direction="column" className={classes.title}>
        <Typography
          variant="subtitle1"
          className={classes.subtitleText}
          align="justify"
        >
          {`${information.role} - ${information.enterprise}`}
        </Typography>
        <Typography
          variant="subtitle1"
          className={classes.subtitleText}
          align="justify"
        >
          {information.endDate
            ? `${information.startDate.split('-').reverse().join('/')
            } - ${
              information.endDate.split('-').reverse().join('/')}`
            : `${information.startDate.split('-').reverse().join('/')
            } - Actualidad`}
        </Typography>
        <Typography
          variant="subtitle2"
          className={classes.subtitleText}
          align="justify"
        >
          {information.description}
        </Typography>
      </Grid>
    )
  );

  const educationExperienceContent = educationInformation.map((information) => (
    <Grid key={information.id} item container direction="column" className={classes.title}>
      <Typography
        variant="subtitle1"
        className={classes.subtitleText}
        align="justify"
      >
        {`${information.role} - ${information.enterprise}`}
      </Typography>
      <Typography
        variant="subtitle1"
        className={classes.subtitleText}
        align="justify"
      >
        {information.endDate
          ? `${information.startDate.split('-').reverse().join('/')
          } - ${
            information.endDate.split('-').reverse().join('/')}`
          : `${information.startDate.split('-').reverse().join('/')
          } - Actualidad`}
      </Typography>
      <Typography
        variant="subtitle2"
        className={classes.subtitleText}
        align="justify"
      >
        {information.description}
      </Typography>
    </Grid>
  ));

  return (
    <>
      <Helmet>
        <title>Candidates List Details | Voting System</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        {location.state && location.state.type !== 'view' ? (
          <Box sx={{
            m: 3
          }}
          >
            <Typography
              color="textPrimary"
              variant="h2"
            >
              {profile ? 'Perfil Actual' : 'Perfil Solicitado' }
            </Typography>
          </Box>
        ) : (
          <Grid />
        )}
        {location.state && location.state.type !== 'view' ? (
          <Box sx={{
            m: 3
          }}
          >
            <Button
              color="inherit"
              fullWidth
              size="large"
              type="submit"
              variant="contained"
              onClick={() => { changeProfile(); }}
            >
              {profile ? 'Ver Cambios Realizados' : 'Ver Perfil Actual' }
            </Button>
          </Box>
        ) : (
          <Grid />
        )}
        <Box sx={{
          m: 3,
          alignItems: 'center',
          display: 'flex'
        }}
        >
          <Avatar
            src={candidate ? candidate.photo : null}
            sx={{ mr: 2 }}
            className={classes.avatar}
          >
            {getInitials(candidate ? `${candidate.names} ${candidate.lastNames}` : '')}
          </Avatar>
          <Typography
            color="textPrimary"
            variant="h1"
          >
            {candidate ? `${candidate.names} ${candidate.lastNames}` : null}
          </Typography>
        </Box>
        <Box sx={{
          m: 3
        }}
        >
          <Typography
            color="textPrimary"
            variant="h2"
          >
            Presentación
          </Typography>
        </Box>
        <Box sx={{
          m: 3,
        }}
        >
          <Typography>{presentation ? presentation.description : ''}</Typography>
        </Box>
        <Grid container direction="column" sx={{ p: 3 }}>
          <Box>
            <Typography variant="h2">Experiencia Profesional</Typography>
          </Box>
          <Grid
            container
            style={{ marginTop: '1vh' }}
          >
            {professionalExperienceContent[0] ? (
              professionalExperienceContent
            ) : (
              <Grid>
                <Typography variant="subtitle1" className={classes.subtitleText}>
                  No ha registrado experiencias profesionales
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
        <Grid container direction="column" sx={{ p: 3 }}>
          <Box>
            <Typography variant="h2">Educación</Typography>
          </Box>
          <Grid
            container
            style={{ marginTop: '1vh' }}
          >
            {educationExperienceContent[0] ? (
              educationExperienceContent
            ) : (
              <Grid>
                <Typography variant="subtitle1" className={classes.subtitleText}>
                  No ha registrado su información académica
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
        {location.state && location.state.type === 'view' ? (
          <Grid item container style={{ margin: '3vh' }} direction="row" alignContent="center" justifyContent="center">
            <Grid
              xs={4}
              item
              container
              style={{ margin: '2vw' }}
            >
              <Button
                color="inherit"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={() => {
                  navigate('/admin/candidates-lists');
                }}
              >
                Volver
              </Button>
            </Grid>
          </Grid>
        ) : (
          <Grid item container style={{ margin: '3vh' }} direction="row" alignContent="center" justifyContent="center">
            <Grid
              xs={4}
              item
              container
              style={{ margin: '2vw' }}
            >
              <Button
                color="secondary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={() => { answerSolicitude(true); }}
              >
                Aceptar Cambios
              </Button>
            </Grid>
            <Grid
              xs={4}
              item
              container
              style={{ margin: '2vw' }}
            >
              <Button
                color="inherit"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                onClick={() => { answerSolicitude(false); }}
              >
                Rechazar Cambios
              </Button>
            </Grid>
          </Grid>
        )}
      </Box>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};

export default CandidatesListDetails;

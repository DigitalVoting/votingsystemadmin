import PropTypes from 'prop-types';
import {
  Box,
  Button,
  TextField,
  InputAdornment,
  SvgIcon,
  Grid,
  Typography
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import ElementCard from 'src/components/Lists/ElementCard';
import { useState } from 'react';

const GetDescription = (element, attributes) => {
  let description = '';
  // eslint-disable-next-line no-restricted-syntax
  for (const variable of attributes) {
    description = description.concat(element[variable]).concat(' ');
  }
  return description;
};

const mapElements = (elements, descriptionAttributes, elementImg, elementClickFunction) => elements.map((element) => (
  <ElementCard
    key={element.id}
    element={element}
    description={GetDescription(element, descriptionAttributes)}
    imgVar={elementImg}
    onClick={() => { elementClickFunction(element); }}
    xs={2}
    md={1}
  />
));

const onKeyUp = (event, findActives, findInactives, findText) => {
  if (event.charCode === 13) {
    findActives(findText);
    findInactives(findText);
  }
};

const ListPage = ({
  title, activeElements, inactiveElements, elementClickFunction,
  descriptionAttributes, elementImg, buttonFunction,
  buttonTitle, secondButton, secondButtonFunction, findActives, findInactives, ...rest
}) => {
  const [searchText, setSearchText] = useState('');

  return (
    <Box {...rest}>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end',
          mt: 2,
          mr: 2,
        }}
      >
        {secondButton
          ? (
            <Button sx={{ mx: 1 }} onClick={() => { secondButtonFunction(); }}>
              Importar
            </Button>
          )
          : (
            <Grid />
          )}
        <Button
          color="secondary"
          variant="contained"
          onClick={() => { buttonFunction(); }}
        >
          {buttonTitle}
        </Button>
      </Box>
      <Box sx={{ m: 3 }}>
        <Typography variant="h1">{title}</Typography>
      </Box>
      <Box sx={{ m: 3 }}>
        <Box sx={{ maxWidth: 500 }}>
          <TextField
            fullWidth
            value={searchText}
            onChange={(event) => {
              setSearchText(event.target.value);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SvgIcon
                    fontSize="small"
                    color="action"
                  >
                    <SearchIcon />
                  </SvgIcon>
                </InputAdornment>
              )
            }}
            placeholder="Buscar"
            variant="outlined"
            onKeyPress={(event) => { onKeyUp(event, findActives, findInactives, searchText); }}
          />
        </Box>
      </Box>
      <Grid container direction="column" sx={{ p: 3 }}>
        <Box>
          <Typography variant="h3">{`${title} Activos`}</Typography>
        </Box>
        <Grid container style={{ overflowX: 'auto', flexWrap: 'nowrap', scrollbarWidth: 'none' }}>
          {mapElements(activeElements, descriptionAttributes, elementImg, elementClickFunction)}
          {activeElements.length > 0
            ? (<Grid />)
            : (
              <Typography variant="h3" m={5}>No se encontraron registros</Typography>
            )}
        </Grid>
      </Grid>
      <Grid container direction="column" sx={{ p: 3 }}>
        <Box>
          <Typography variant="h3">{`${title} Inactivos`}</Typography>
        </Box>
        <Grid container style={{ overflowX: 'auto', flexWrap: 'nowrap', scrollbarWidth: 'none' }}>
          {mapElements(inactiveElements, descriptionAttributes, elementImg, elementClickFunction)}
          {inactiveElements.length > 0
            ? (<Grid />)
            : (
              <Typography variant="h3" m={5}>No se encontraron registros</Typography>
            )}
        </Grid>
      </Grid>
    </Box>
  );
};

ListPage.propTypes = {
  title: PropTypes.string.isRequired,
  activeElements: PropTypes.array.isRequired,
  inactiveElements: PropTypes.array.isRequired,
  elementClickFunction: PropTypes.func.isRequired,
  descriptionAttributes: PropTypes.array.isRequired,
  elementImg: PropTypes.string.isRequired,
  buttonFunction: PropTypes.func.isRequired,
  buttonTitle: PropTypes.string.isRequired,
  secondButton: PropTypes.bool.isRequired,
  secondButtonFunction: PropTypes.func,
  findActives: PropTypes.func.isRequired,
  findInactives: PropTypes.func.isRequired
};

export default ListPage;

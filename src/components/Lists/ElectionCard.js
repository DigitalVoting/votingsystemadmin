import PropTypes from 'prop-types';
import {
  Card,
  CardHeader,
  CardMedia,
  Grid,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import theme from 'src/theme';

const useStyles = makeStyles(() => ({
  root: {
    minWidth: 325,
    maxWidth: 325,
    border: '1px solid',
    borderColor: theme.palette.secondary.main,
    borderRadius: '12px',
    cursor: 'pointer'
  },
  media: {
    height: 0,
    padding: theme.spacing(18),
  },
  header: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
}));

const ElectionCard = ({
  element, imgVar, title, clickFunction, ...rest
}) => {
  const classes = useStyles();

  return (
    <Grid
      item
      container
      direction="column"
      {...rest}
      sx={{
        m: 5,
        mb: 0,
      }}
    >
      <Card className={classes.root} key={element.id} onClick={() => { clickFunction(element); }}>
        <CardMedia
          className={classes.media}
          image={element[imgVar]}
          title={element[title]}
        />
        <CardHeader
          disableTypography
          title={<Typography variant="h6">{element[title]}</Typography>}
          className={classes.header}
        />
      </Card>
    </Grid>
  );
};

ElectionCard.propTypes = {
  element: PropTypes.object.isRequired,
  imgVar: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  clickFunction: PropTypes.func.isRequired,
};

export default ElectionCard;

import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  lista: {
    width: '120px !important',
    height: '120px !important',
  },
  card: {
    cursor: 'pointer'
  },
}));

const ElementCard = ({
  element, description, imgVar, ...rest
}) => {
  const classes = useStyles();

  return (
    <Grid
      item
      container
      direction="column"
      {...rest}
      sx={{
        m: 5,
        mb: 0,
      }}
    >
      <Card
        sx={{
          display: 'flex',
          flexDirection: 'column',
        }}
        className={classes.card}
      >
        <CardContent sx={{ background: '#851610' }}>
          <Grid container direction="column" style={{ alignContent: 'center', justifyContent: 'center' }}>
            <Avatar
              alt={description}
              src={element[imgVar]}
              className={classes.lista}
            />
          </Grid>
        </CardContent>
        <Box sx={{ flexGrow: 1 }} />
      </Card>
      <Typography
        align="center"
        color="textPrimary"
        gutterBottom
        variant="h4"
        mt={2}
      >
        {`${description}`}
      </Typography>
    </Grid>
  );
};

ElementCard.propTypes = {
  element: PropTypes.object.isRequired,
  imgVar: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default ElementCard;

import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import PropTypes from 'prop-types';

const TotalCustomers = ({ numberElements, label, ...rest }) => (
  <Card {...rest}>
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}
      >
        <Grid item>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="h6"
          >
            {label.toUpperCase()}
          </Typography>
          <Typography
            color="textPrimary"
            variant="h3"
          >
            {numberElements}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: '#851610',
              height: 56,
              width: 56
            }}
          >
            <PeopleIcon />
          </Avatar>
        </Grid>
      </Grid>
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          pt: 2
        }}
      >
        <Typography
          color="textSecondary"
          variant="caption"
        >
          Activos en la elección actual
        </Typography>
      </Box>
    </CardContent>
  </Card>
);

TotalCustomers.propTypes = {
  numberElements: PropTypes.number,
  label: PropTypes.string
};

export default TotalCustomers;

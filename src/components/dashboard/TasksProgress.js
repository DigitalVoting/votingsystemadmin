import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  LinearProgress,
  Typography
} from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import InsertChartIcon from '@material-ui/icons/InsertChartOutlined';
import PropTypes from 'prop-types';

const TasksProgress = ({ percentage, label, ...rest }) => (
  <Card
    sx={{ height: '100%' }}
    {...rest}
  >
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}
      >
        <Grid item>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="h6"
          >
            {label.toUpperCase()}
          </Typography>
          <Typography
            color="textPrimary"
            variant="h3"
          >
            {`${percentage}%`}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: orange[600],
              height: 56,
              width: 56
            }}
          >
            <InsertChartIcon />
          </Avatar>
        </Grid>
      </Grid>
      <Box sx={{ pt: 3 }}>
        <LinearProgress
          value={percentage}
          variant="determinate"
        />
      </Box>
    </CardContent>
  </Card>
);

TasksProgress.propTypes = {
  percentage: PropTypes.number,
  label: PropTypes.string
};

export default TasksProgress;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DateTimePicker from '@material-ui/lab/DateTimePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import esPELocale from 'date-fns/locale/es';

import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';
import { Alarm, CalendarToday } from '@material-ui/icons';

const useStyles = makeStyles(() => ({
  root: {
    color: 'black'
  },
}));

function DatePickerStage({
  selectedDate, handleDateChange, label, minDate
}) {
  const classes = useStyles();
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} locale={esPELocale}>
      <DateTimePicker
        label={label}
        value={selectedDate}
        onChange={handleDateChange}
        minDate={minDate}
        className={classes.root}
        renderInput={(params) => <TextField {...params} />}
        timeIcon={<Alarm color="secondary" />}
        dateRangeIcon={<CalendarToday color="secondary" />}
      />
    </LocalizationProvider>
  );
}
DatePickerStage.propTypes = {
  selectedDate: PropTypes.object,
  handleDateChange: PropTypes.func,
  label: PropTypes.string,
  minDate: PropTypes.object,
};
export default DatePickerStage;

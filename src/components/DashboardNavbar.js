import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import {
  AppBar,
  Box,
  IconButton,
  Toolbar,
  Typography
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import Hidden from '@material-ui/core/Hidden';
import InputIcon from '@material-ui/icons/Input';
import { AuthContext } from '../context/auth-context';

const DashboardNavbar = ({ onMobileNavOpen, ...rest }) => {
  const navigate = useNavigate();
  const authContext = React.useContext(AuthContext);
  return (
    <AppBar
      elevation={2}
      {...rest}
      color="inherit"
    >
      <Toolbar>
        <RouterLink to="/">
          <Typography
            align="center"
            variant="h4"
            color="textPrimary"
          >
            Voting System Admin
          </Typography>
        </RouterLink>
        <Box sx={{ flexGrow: 1 }} />
        <Hidden lgDown>
          <IconButton
            color="inherit"
            onClick={() => {
              localStorage.setItem('userAuth', false);
              localStorage.removeItem('userAuth');
              localStorage.removeItem('user');
              authContext.signout();
              navigate('/login', { replace: true });
            }}
          >
            <InputIcon />
          </IconButton>
        </Hidden>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onMobileNavOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

DashboardNavbar.propTypes = {
  onMobileNavOpen: PropTypes.func
};

export default DashboardNavbar;

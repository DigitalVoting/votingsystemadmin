import { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';
import getInitials from 'src/utils/getInitials';

const CustomerListResults = ({ customers, rowFunction, ...rest }) => {
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Usuario Relacionado
                </TableCell>
                <TableCell>
                  Solicitud
                </TableCell>
                <TableCell>
                  Descripción
                </TableCell>
                <TableCell>
                  Lista Relacionada
                </TableCell>
                <TableCell>
                  Fecha Solicitud
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice((page - 1) * limit, limit).map((customer) => (
                <TableRow
                  hover
                  key={customer.id}
                  style={{ cursor: 'pointer' }}
                  onClick={() => { rowFunction(customer); }}
                >
                  <TableCell>
                    <Box
                      sx={{
                        alignItems: 'center',
                        display: 'flex'
                      }}
                    >
                      <Avatar
                        src={customer.user.photo}
                        sx={{ mr: 2 }}
                      >
                        {getInitials(customer.user.names + customer.user.lastNames)}
                      </Avatar>
                      <Typography
                        color="textPrimary"
                        variant="body1"
                      >
                        {customer.user.names + customer.user.lastNames}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell>
                    {customer.title}
                  </TableCell>
                  <TableCell>
                    {customer.description}
                  </TableCell>
                  <TableCell>
                    {customer.candidatesList.name}
                  </TableCell>
                  <TableCell>
                    {moment(customer.creationDate).format('DD/MM/YYYY')}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

CustomerListResults.propTypes = {
  customers: PropTypes.array.isRequired,
  rowFunction: PropTypes.func.isRequired,
};

export default CustomerListResults;

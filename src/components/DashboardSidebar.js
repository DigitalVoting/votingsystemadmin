import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Divider,
  Drawer,
  List,
  Typography
} from '@material-ui/core';
import Hidden from '@material-ui/core/Hidden';
import {
  Home as HomeIcon,
  Briefcase as BriefcaseIcon,
  LogOut as LogOutIcon,
  Users as UsersIcon,
  List as ListIcon,
  Inbox as InboxIcon,
} from 'react-feather';
import { AuthContext } from 'src/context/auth-context';
import NavItem from './NavItem';

const items = [
  {
    href: '/admin/home',
    icon: HomeIcon,
    title: 'Inicio'
  },
  {
    href: '/admin/electors',
    icon: UsersIcon,
    title: 'Electores'
  },
  {
    href: '/admin/elections',
    icon: ListIcon,
    title: 'Elección'
  },
  {
    href: '/admin/candidates-lists',
    icon: BriefcaseIcon,
    title: 'Listas de Candidatos'
  },
  {
    href: '/admin/solicitudes',
    icon: InboxIcon,
    title: 'Solicitudes'
  },
  {
    href: '/login',
    icon: LogOutIcon,
    title: 'Cerrar sesión'
  }

];

const DashboardSidebar = ({ onMobileClose, openMobile }) => {
  const location = useLocation();
  const authContext = React.useContext(AuthContext);
  const user = JSON.parse(localStorage.getItem('user'));
  const navigate = useNavigate();
  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
  }, [location.pathname]);

  const content = (
    <Box
      sx={{
        display: 'flex',
        backgroundColor: 'primary.main',
        flexDirection: 'column',
        height: '100%'
      }}
    >
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
          p: 2
        }}
      >
        <Avatar
          component={RouterLink}
          src={user.photo}
          sx={{
            cursor: 'pointer',
            width: 64,
            height: 64
          }}
          to="/admin/inicio"
        />
        <Typography
          sx={{ color: 'primary.contrastText' }}
          variant="h5"
          mt={2}
        >
          {`${user.names} ${user.lastNames}`}
        </Typography>
        <Typography
          sx={{ color: 'secondary.main' }}
          variant="body2"
        >
          Responsable de las Elecciones
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        <List>
          {items.map((item) => (
            <NavItem
              href={item.href}
              key={item.title}
              title={item.title}
              icon={item.icon}
              onClick={item.href === '/login' ? (() => {
                localStorage.setItem('userAuth', false);
                localStorage.removeItem('userAuth');
                localStorage.removeItem('user');
                authContext.signout();
                navigate('/login', { replace: true });
              }) : null}
            />
          ))}
        </List>
      </Box>
      <Box sx={{ flexGrow: 1 }} />
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
          PaperProps={{
            sx: {
              width: 256
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden lgDown>
        <Drawer
          anchor="left"
          open
          variant="persistent"
          PaperProps={{
            sx: {
              width: 256,
              top: 64,
              height: 'calc(100% - 64px)'
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
};

DashboardSidebar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool
};

DashboardSidebar.defaultProps = {
  onMobileClose: () => {},
  openMobile: false
};

export default DashboardSidebar;

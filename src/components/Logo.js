import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  logo: {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '25%',
    overflow: 'hidden',
    height: '50%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
}));
const Logo = (props) => {
  const classes = useStyles();
  return (
    <img
      alt="Logo"
      src="/static/logo.svg"
      className={classes.logo}
      {...props}
    />
  );
};

export default Logo;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const ButtonImport = (props) => {
  const classes = useStyles();
  const { handleInputChange } = props;
  return (
    <div className={classes.root}>
      <input
        accept=".xlsx"
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
        onChange={(e) => handleInputChange(e)}
      />
      {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span">
          Seleccionar archivo
        </Button>
      </label>
    </div>
  );
};
ButtonImport.propTypes = {
  handleInputChange: PropTypes.func,
};

export default ButtonImport;

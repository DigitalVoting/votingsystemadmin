import { useState } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
} from '@material-ui/core';

const MassiveChargeTable = (props) => {
  const { dataTable, headCells } = props;
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [init, setInit] = useState(0);
  const [end, setEnd] = useState(10);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
    let newInit; let
      newEnd;
    if (newPage > page) {
      newInit = init + limit;
      newEnd = end + limit;
    } else {
      newInit = init - limit;
      newEnd = end - limit;
    }
    setInit(newInit);
    setEnd(newEnd);
  };

  return (
    <Card>
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                {headCells.map((hCell) => (
                  <TableCell key={hCell.id}>
                    {hCell.name}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {dataTable.slice(init, end).map((element) => {
                const cellsData = [];
                for (let obj = 0; obj < headCells.length; obj++) {
                  const labelCell = headCells[obj].value;
                  cellsData.push(
                    <TableCell key={obj}>
                      {element[labelCell]}
                    </TableCell>
                  );
                }
                return (
                  <TableRow
                    hover
                    key={element.id}
                  >
                    {cellsData}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={dataTable.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

MassiveChargeTable.propTypes = {
  dataTable: PropTypes.array.isRequired,
  headCells: PropTypes.array.isRequired,
};

export default MassiveChargeTable;

import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import paperStyle from '../../theme/paper.css';

function AlertMod(props) {
  return <Alert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  snackbar: {
    backgroundColor: 'transparent !important'
  },
}));

const SnackbarComponent = ({
  message, handleClose, severity, open
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Snackbar
        id="snackbar"
        open={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        classes={paperStyle['css-135malt-MuiPaper-root-MuiSnackbarContent-root']}
        action={(
          <>
            <AlertMod onClose={handleClose} severity={severity}>
              {message}
            </AlertMod>
          </>
        )}
      />
    </div>
  );
};

SnackbarComponent.propTypes = {
  message: PropTypes.string,
  handleClose: PropTypes.func,
  severity: PropTypes.string,
  open: PropTypes.bool,
};
export default SnackbarComponent;

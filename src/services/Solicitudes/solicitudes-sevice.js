import axios from 'axios';
import authHeader from '../Authentication/auth-header';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}notification/`;

const getNotifications = (userId, candidateListId) => axios.post(
  `${API_URL}list-active`,
  {
    userId,
    candidateListId,
  },
  {
    headers: authHeader(),
  }
);

const getSolicitudes = () => axios.post(
  `${API_URL}list-solicitudes`,
  {},
  {
    headers: authHeader(),
  }
);

export default {
  getNotifications,
  getSolicitudes,
};

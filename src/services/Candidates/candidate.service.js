import axios from 'axios';
import authHeader from '../Authentication/auth-header';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}user/candidates/`;

const getCandidatesCurrent = () => axios.post(
  `${API_URL}listcurrent`,
  {},
  {
    headers: authHeader(),
  }
);

const findCandidatesCurrent = (find) => axios.post(
  `${API_URL}find`,
  {
    name: find,
  },
  {
    headers: authHeader(),
  }
);

export default {
  getCandidatesCurrent,
  findCandidatesCurrent,
};

import axios from 'axios';
import authHeader from '../Authentication/auth-header';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}information/`;

const getCandidateInformation = (candidateId, listId, status) => axios.post(
  `${API_URL}candidate`,
  {
    userId: candidateId,
    candidateListId: listId,
    candidateInformation: [],
    status,
  },
  {
    headers: authHeader(),
  }
);

const saveCandidateInformation = (
  candidateId,
  listId,
  status,
  candidateInformation
) => axios.post(
  `${API_URL}saveInformation`,
  {
    userId: candidateId,
    candidateListId: listId,
    status,
    candidateInformation,
  },
  {
    headers: authHeader(),
  }
);

const acceptChanges = (userId, candidateListId, creationUserId, status, notificationId) => axios.post(
  `${API_URL}accept-changes`,
  {
    userId,
    candidateListId,
    creationUserId,
    status,
    notificationId,
  },
  {
    headers: authHeader(),
  }
);

export default {
  getCandidateInformation,
  saveCandidateInformation,
  acceptChanges,
};

import axios from 'axios';
import authHeader from '../Authentication/auth-header';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}candidateslist/`;

const getCandidatesLists = (id) => axios.post(
  `${API_URL}listall?id=${id}`,
  {},
  {
    headers: authHeader(),
  }
);

const getCandidatesListsCurrent = () => axios.post(
  `${API_URL}listcurrent`,
  {},
  {
    headers: authHeader(),
  }
);

const getCandidatesOfCandidatesList = (id) => axios.post(
  `${API_URL}candidates`,
  {
    userId: 0,
    candidateListId: id,
    candidateInformation: [],
  },
  {
    headers: authHeader(),
  }
);

const findCandidatesListsCurrent = (find) => axios.post(
  `${API_URL}find`,
  {
    name: find,
  },
  {
    headers: authHeader(),
  }
);

const findInactiveCandidatesLists = (find) => axios.post(
  `${API_URL}inactive`,
  {
    name: find,
  },
  {
    headers: authHeader(),
  }
);

const updateCandidateList = (
  candidatesListId,
  listName,
  description,
  logoUrl,
  status,
  updateUser,
  type,
  notificationId
) => axios.post(
  `${API_URL}update`,
  {
    candidatesListId,
    name: listName,
    description,
    logoUrl,
    status,
    updateUser,
    type,
    notificationId
  },
  {
    headers: authHeader(),
  }
);

const saveCandidatesList = (
  candidatesList,
  candidates,
  userId,
  status,
  electionId
) => axios.post(
  `${API_URL}save`,
  {
    name: candidatesList.name,
    description: candidatesList.description,
    status,
    logoUrl: candidatesList.logo,
    creationUser: userId,
    users: candidates,
    election: electionId,
  },
  {
    headers: authHeader(),
  }
);

const answerSolicitude = (userId, candidatesListId, answer, notificationId) => axios.post(
  `${API_URL}answer-solicitude`,
  {
    userId,
    candidatesListId,
    answer,
    notificationId,
  },
  {
    headers: authHeader(),
  }
);

export default {
  getCandidatesLists,
  getCandidatesListsCurrent,
  getCandidatesOfCandidatesList,
  findCandidatesListsCurrent,
  updateCandidateList,
  saveCandidatesList,
  answerSolicitude,
  findInactiveCandidatesLists,
};

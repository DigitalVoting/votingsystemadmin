import axios from 'axios';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}auth/`;

const register = (username, names, lastNames, email, creationUser, roles, password) => axios.post(`${API_URL}signup`, {
  username,
  names,
  lastNames,
  email,
  creationUser,
  roles,
  password,
});

const signIn = (username, password) => axios
  .post(`${API_URL}signin`, {
    username,
    password,
  })
  .then((response) => {
    if (response.data.accessToken && response.data.roles.includes('ROLE_ADMIN')) {
      console.log('Admin Login');
      localStorage.setItem('user', JSON.stringify(response.data));
      localStorage.setItem('userAuth', true);
    }

    return response.data;
  });

const signOut = () => {
  localStorage.setItem('userAuth', false);
  localStorage.removeItem('userAuth');
  localStorage.removeItem('user');
  localStorage.removeItem('Fecha');
};

const restorePassword = (username) => axios
  .post(`${API_URL}restore-password`, {
    username,
    password: 'password',
  })
  .then((response) => response.data);

const confirmToken = (userId, token) => axios
  .post(`${API_URL}confirm-token`, {
    userId,
    password: 'password',
    token,
  })
  .then((response) => response.data);

const changePassword = (userId, password, token) => axios
  .post(`${API_URL}change-password`, {
    userId,
    password,
    token,
  })
  .then((response) => response.data);

export default {
  register,
  signIn,
  signOut,
  restorePassword,
  confirmToken,
  changePassword,
};

import axios from 'axios';
import * as API from '../serverApi';
import authHeader from '../Authentication/auth-header';

const API_URL = `${API.API_URL_ORIGIN}user/`;

const getActiveUsers = () => axios.post(`${API_URL}get-active`, {
},
{
  headers: authHeader(),
});

const findActiveUsers = (find) => axios.post(`${API_URL}find-active`, {
  name: find
},
{
  headers: authHeader(),
});

const getInactiveUsers = (find) => axios.post(`${API_URL}inactive`, {
  name: find
},
{
  headers: authHeader(),
});

const saveElectorsList = (electorsRequest) => axios.post(
  `${API_URL}register-list`,
  electorsRequest,
  { headers: authHeader() }
);

export default {
  getActiveUsers,
  findActiveUsers,
  getInactiveUsers,
  saveElectorsList,
};

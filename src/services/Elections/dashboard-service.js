import axios from 'axios';
import authHeader from '../Authentication/auth-header';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}dashboard/`;

const diaryVotes = () => axios.post(
  `${API_URL}diary-votes`,
  {},
  {
    headers: authHeader(),
  }
);

const diaryElectoralParticipation = () => axios.post(
  `${API_URL}diary-electoral-participation`,
  {},
  {
    headers: authHeader(),
  }
);

const electoralParticipation = () => axios.post(
  `${API_URL}electoral-participation`,
  {},
  {
    headers: authHeader(),
  }
);

const totalElectors = () => axios.post(
  `${API_URL}total-electors`,
  {},
  {
    headers: authHeader(),
  }
);

export default {
  diaryVotes,
  diaryElectoralParticipation,
  electoralParticipation,
  totalElectors,
};

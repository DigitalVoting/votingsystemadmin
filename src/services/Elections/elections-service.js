import axios from 'axios';
import authHeader from '../Authentication/auth-header';
import * as API from '../serverApi';

const API_URL = `${API.API_URL_ORIGIN}election/`;

const activeElection = () => axios.post(
  `${API_URL}active-election`,
  {},
  {
    headers: authHeader(),
  }
);

const getInactiveElections = (find) => axios.post(
  `${API_URL}find-inactives`,
  {
    name: find
  },
  {
    headers: authHeader(),
  }
);

const getActiveElections = (find) => axios.post(
  `${API_URL}find-actives`,
  {
    name: find
  },
  {
    headers: authHeader(),
  }
);

const getStage = () => axios.post(
  `${API_URL}active-election-stage`,
  {},
  {
    headers: authHeader(),
  }
);

const generateToken = (userId, electionId) => axios.post(
  `${API_URL}generate-token`,
  {
    userId,
    electionId,
  },
  {
    headers: authHeader(),
  }
);

const hasVoted = (userId, electionId) => axios.post(
  `${API_URL}has-voted`,
  {
    userId,
    electionId,
  },
  {
    headers: authHeader(),
  }
);

const confirmToken = (userId, electionId, token) => axios.post(
  `${API_URL}confirm-token`,
  {
    userId,
    electionId,
    token,
  },
  {
    headers: authHeader(),
  }
);

const getResults = (electionId) => axios.post(
  `${API_URL}results`,
  {
    id: electionId,
  },
  {
    headers: authHeader(),
  }
);

const createElection = (election) => axios.post(
  `${API_URL}election`,
  {
    ...election
  },
  {
    headers: authHeader(),
  }
);

const getElectionStage = (id) => axios.post(
  `${API_URL}election-stage`,
  {
    id,
  },
  {
    headers: authHeader(),
  }
);

const updateElection = (election) => axios.post(
  `${API_URL}update`,
  {
    ...election
  },
  {
    headers: authHeader(),
  }
);

export default {
  activeElection,
  createElection,
  updateElection,
  getInactiveElections,
  getActiveElections,
  getStage,
  generateToken,
  hasVoted,
  confirmToken,
  getResults,
  getElectionStage,
};

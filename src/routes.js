import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/components/DashboardLayout';
import MainLayout from 'src/components/MainLayout';
import Elections from 'src/pages/Elections';
import Electors from 'src/pages/Electors';
import CandidatesLists from 'src/pages/CandidatesList';
import Dashboard from 'src/pages/Dashboard';
import NotFound from 'src/pages/NotFound';
import NewElection from 'src/pages/NewElection';
import Solicitudes from './pages/Solicitudes';
import ElectionDetails from './pages/ElectionDetails';
import CandidatesListDetails from './pages/CandidatesListDetails';
import CandidateDetails from './pages/CandidateDetails';
import Results from './pages/Results';

const routes = [
  {
    path: 'admin',
    element: <DashboardLayout />,
    children: [
      { path: 'home', element: <Dashboard /> },
      { path: 'electors', element: <Electors /> },
      { path: 'elections', element: <Elections /> },
      { path: 'elections/new', element: <NewElection /> },
      { path: 'elections/details', element: <ElectionDetails /> },
      { path: 'elections/results', element: <Results /> },
      { path: 'candidates-lists', element: <CandidatesLists /> },
      { path: 'candidates-lists/details', element: <CandidatesListDetails /> },
      { path: 'candidates/details', element: <CandidateDetails /> },
      { path: 'solicitudes', element: <Solicitudes /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: '404', element: <NotFound /> },
      { path: '/login', element: <Navigate to="/admin/home" /> },
      { path: '/', element: <Navigate to="/admin/home" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];
export default routes;

import React from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { Navigate, useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/components/GlobalStyles';
import 'src/mixins/chartjs';
import theme from 'src/theme';
import routes from 'src/routes';
import { AuthContext } from './context/auth-context';
import Login from './pages/Login';
import MainLayout from './components/MainLayout';

const notLoggedRoutes = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <Login /> },
      { path: '/', element: <Navigate to="/login" /> },
      { path: '*', element: <Navigate to="/login" /> }
    ]
  }
];

const App = () => {
  const authContext = React.useContext(AuthContext);
  const routing = authContext.isAuth ? useRoutes(routes) : useRoutes(notLoggedRoutes);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {routing}
    </ThemeProvider>
  );
};

export default App;

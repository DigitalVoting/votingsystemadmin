import { colors } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';
import shadows from './shadows';
import typography from './typography';

const theme = createTheme({
  palette: {
    background: {
      default: '#F4F6F8',
      paper: colors.common.white
    },
    primary: {
      light: '#94F5D3',
      main: '#851610',
      dark: '#961610',
      contrastText: '#FFFFFF',
    },
    secondary: {
      light: '#528EA9',
      main: '#D1B059',
      dark: '#D0A059',
      contrastText: '#FFFFFF',
    },
    text: {
      primary: '#00374E',
      secondary: '#000000'
    },
    error: {
      main: '#CF433A',
    },
  },
  shadows,
  typography
});

export default theme;
